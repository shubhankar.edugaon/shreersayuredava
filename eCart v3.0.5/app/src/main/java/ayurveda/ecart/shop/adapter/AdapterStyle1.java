package ayurveda.ecart.shop.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import ayurveda.ecart.shop.R;
import ayurveda.ecart.shop.fragment.ProductDetailFragment;
import ayurveda.ecart.shop.helper.ApiConfig;
import ayurveda.ecart.shop.helper.Constant;
import ayurveda.ecart.shop.helper.DatabaseHelper;
import ayurveda.ecart.shop.helper.Session;
import ayurveda.ecart.shop.model.Product;
import ayurveda.ecart.shop.model.Variants;

/**
 * Created by shree1 on 3/16/2017.
 */

public class AdapterStyle1 extends RecyclerView.Adapter<AdapterStyle1.ItemHolder> {

    public final ArrayList<Product> productList;
    public final Activity activity;
    public final int itemResource;
    Session session;
    boolean isLogin;
    DatabaseHelper databaseHelper;
    long available_stock;
    HashMap<String, Long> hashMap;

    public AdapterStyle1(Activity activity, ArrayList<Product> productList, int itemResource, HashMap<String, Long> hashMap) {
        this.activity = activity;
        this.session = new Session(activity);
        this.databaseHelper = new DatabaseHelper(activity);
        isLogin = session.getBoolean(Constant.IS_USER_LOGIN);
        this.productList = productList;
        this.itemResource = itemResource;
        this.hashMap = hashMap;
        available_stock = 0;
    }

    @Override
    public int getItemCount() {
        return Math.min(productList.size(), 4);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ItemHolder holder, final int position) {
        final Product product = productList.get(position);
        Variants variant = product.getVariants().get(0);

        String maxCartCont;

        if (product.getTotal_allowed_quantity() == null || product.getTotal_allowed_quantity().equals("") || product.getTotal_allowed_quantity().equals("0")) {
            maxCartCont = session.getData(Constant.max_cart_items_count);
        } else {
            maxCartCont = product.getTotal_allowed_quantity();
        }

        if (variant.getServe_for().equalsIgnoreCase(Constant.SOLD_OUT_TEXT)) {
            holder.tvStatus.setVisibility(View.VISIBLE);
            holder.lytQuantity.setVisibility(View.GONE);
        } else {
            holder.tvStatus.setVisibility(View.GONE);
            holder.lytQuantity.setVisibility(View.VISIBLE);
        }


            Picasso.get().
                    load((product.getImage() == null || product.getImage().equals(""))?"-":product.getImage())
                    .fit()
                    .centerInside()
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(holder.imgProduct);

        holder.tvTitle.setText(product.getName());

        double price, oPrice;
        String taxPercentage = "0";
        try {
            taxPercentage = (Double.parseDouble(product.getTax_percentage()) > 0 ? product.getTax_percentage() : "0");
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (variant.getDiscounted_price().equals("0") || variant.getDiscounted_price().equals("")) {
            holder.tvDPrice.setVisibility(View.GONE);
            price = ((Float.parseFloat(variant.getPrice()) + ((Float.parseFloat(variant.getPrice()) * Float.parseFloat(taxPercentage)) / 100)));
        } else {
            price = ((Float.parseFloat(variant.getDiscounted_price()) + ((Float.parseFloat(variant.getDiscounted_price()) * Float.parseFloat(taxPercentage)) / 100)));
            oPrice = (Float.parseFloat(variant.getPrice()) + ((Float.parseFloat(variant.getPrice()) * Float.parseFloat(taxPercentage)) / 100));

            holder.tvDPrice.setPaintFlags(holder.tvDPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.tvDPrice.setText(new Session(activity).getData(Constant.CURRENCY) + ApiConfig.StringFormat("" + oPrice));

            holder.tvDPrice.setVisibility(View.VISIBLE);
        }
        holder.tvPrice.setText(new Session(activity).getData(Constant.CURRENCY) + ApiConfig.StringFormat("" + price));

        holder.tvTitle.setText(product.getName());

        holder.relativeLayout.setOnClickListener(view -> {
            AppCompatActivity activity1 = (AppCompatActivity) activity;
            Fragment fragment = new ProductDetailFragment();
            Bundle bundle = new Bundle();
            bundle.putString(Constant.ID, product.getVariants().get(0).getProduct_id());
            bundle.putString(Constant.FROM, "section");
            bundle.putInt(Constant.VARIANT_POSITION, 0);
            fragment.setArguments(bundle);
            activity1.getSupportFragmentManager().beginTransaction().add(R.id.container, fragment).addToBackStack(null).commit();
        });

        if (isLogin) {
            holder.tvQuantity.setText(variant.getCart_count());
        } else {
            holder.tvQuantity.setText(databaseHelper.CheckCartItemExist(product.getVariants().get(0).getId(), product.getVariants().get(0).getProduct_id()));
        }

        holder.btnAddToCart.setVisibility(holder.tvQuantity.getText().equals("0") ? View.VISIBLE : View.GONE);


        boolean isLoose = variant.getType().equalsIgnoreCase("loose");


        if (isLoose) {
            holder.btnMinusQty.setOnClickListener(view -> removeLooseItemFromCartClickEvent(holder, variant));

            holder.btnAddQty.setOnClickListener(view -> addLooseItemToCartClickEvent(holder, variant, maxCartCont));

            holder.btnAddToCart.setOnClickListener(v -> addLooseItemToCartClickEvent(holder, variant, maxCartCont));
        } else {
            holder.btnMinusQty.setOnClickListener(view -> removeFromCartClickEvent(holder, variant, maxCartCont));

            holder.btnAddQty.setOnClickListener(view -> addToCartClickEvent(holder, variant, maxCartCont));

            holder.btnAddToCart.setOnClickListener(v -> addToCartClickEvent(holder, variant, maxCartCont));
        }
    }

    public void addLooseItemToCartClickEvent(ItemHolder holder, Variants variants, String maxCartCont) {
        long unitMeasurement = (variants.getMeasurement_unit_name().equalsIgnoreCase("kg") || variants.getMeasurement_unit_name().equalsIgnoreCase("ltr")) ? 1000 : 1;
        long unit = (long) (Double.parseDouble(variants.getMeasurement()) * unitMeasurement);
        available_stock = hashMap.get(variants.getProduct_id());
        if (session.getData(Constant.STATUS).equals("1")) {
            int count = Integer.parseInt(holder.tvQuantity.getText().toString());

            if (count <= Integer.parseInt(maxCartCont)) {
                count++;
                if (count != 0) {
                    holder.btnAddToCart.setVisibility(View.GONE);
                }
                if (available_stock >= unit) {
                    if (hashMap.containsKey(variants.getProduct_id())) {
                        hashMap.replace(variants.getProduct_id(), (hashMap.get(variants.getProduct_id()) - unit));
                    } else {
                        hashMap.put(variants.getProduct_id(), unit);
                    }
                    if (isLogin) {
                        if (Constant.CartValues.containsKey(variants.getId())) {
                            Constant.CartValues.replace(variants.getId(), "" + count);
                        } else {
                            Constant.CartValues.put(variants.getId(), "" + count);
                        }
                        ApiConfig.AddMultipleProductInCart(session, activity, Constant.CartValues);
                    } else {
                        databaseHelper.AddToCart(variants.getId(), variants.getProduct_id(), "" + count);
                        databaseHelper.getTotalItemOfCart(activity);
                    }
                    if (count > 0) {
                        holder.btnAddToCart.setVisibility(View.GONE);
                    } else {
                        holder.btnAddToCart.setVisibility(View.VISIBLE);
                    }
                    holder.tvQuantity.setText("" + count);
                } else {
                    Toast.makeText(activity, activity.getString(R.string.stock_limit), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(activity, activity.getString(R.string.limit_alert), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(activity, activity.getString(R.string.user_deactivate_msg), Toast.LENGTH_SHORT).show();
        }
    }

    public void removeLooseItemFromCartClickEvent(ItemHolder holder, Variants variants) {

        long unitMeasurement = (variants.getMeasurement_unit_name().equalsIgnoreCase("kg") || variants.getMeasurement_unit_name().equalsIgnoreCase("ltr")) ? 1000 : 1;
        long unit = (long) Double.parseDouble(variants.getMeasurement()) * unitMeasurement;

        if (session.getData(Constant.STATUS).equals("1")) {
            int count = Integer.parseInt(holder.tvQuantity.getText().toString());
            count--;
            available_stock += unit;
            if (count == 0) {
                holder.btnAddToCart.setVisibility(View.VISIBLE);
            }
            if (isLogin) {
                if (count <= 0) {
                    holder.tvQuantity.setText("" + count);
                    if (Constant.CartValues.containsKey(variants.getId())) {
                        Constant.CartValues.replace(variants.getId(), "" + count);
                    } else {
                        Constant.CartValues.put(variants.getId(), "" + count);
                    }
                    ApiConfig.AddMultipleProductInCart(session, activity, Constant.CartValues);
                }
            } else {
                holder.tvQuantity.setText("" + count);
                databaseHelper.AddToCart(variants.getId(), variants.getProduct_id(), "" + count);
                databaseHelper.getTotalItemOfCart(activity);
            }

        } else {
            Toast.makeText(activity, activity.getString(R.string.user_deactivate_msg), Toast.LENGTH_SHORT).show();
        }
    }

    public void addToCartClickEvent(ItemHolder holder, Variants variants, String maxCartCont) {
        if (session.getData(Constant.STATUS).equals("1")) {
            int count = Integer.parseInt(holder.tvQuantity.getText().toString());
            if (count < Float.parseFloat(variants.getStock())) {
                if (count < Integer.parseInt(maxCartCont)) {
                    count++;
                    if (count != 0) {
                        holder.btnAddToCart.setVisibility(View.GONE);
                    }
                    holder.tvQuantity.setText("" + count);
                    if (isLogin) {
                        if (Constant.CartValues.containsKey(variants.getId())) {
                            Constant.CartValues.replace(variants.getId(), "" + count);
                        } else {
                            Constant.CartValues.put(variants.getId(), "" + count);
                        }
                        ApiConfig.AddMultipleProductInCart(session, activity, Constant.CartValues);
                    } else {
                        holder.tvQuantity.setText("" + count);
                        databaseHelper.AddToCart(variants.getId(), variants.getProduct_id(), "" + count);
                        databaseHelper.getTotalItemOfCart(activity);
                    }
                } else {
                    Toast.makeText(activity, activity.getString(R.string.limit_alert), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(activity, activity.getString(R.string.stock_limit), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(activity, activity.getString(R.string.user_deactivate_msg), Toast.LENGTH_SHORT).show();
        }
    }

    public void removeFromCartClickEvent(ItemHolder holder, Variants variants, String maxCartCont) {
        if (session.getData(Constant.STATUS).equals("1")) {
            int count = Integer.parseInt(holder.tvQuantity.getText().toString());
            if (count <= Float.parseFloat(variants.getStock())) {
                if (count <= Integer.parseInt(maxCartCont)) {
                    count--;
                    if (count == 0) {
                        holder.btnAddToCart.setVisibility(View.VISIBLE);
                    }
                    holder.tvQuantity.setText("" + count);
                    if (isLogin) {
                        if (count <= 0) {
                            holder.tvQuantity.setText("" + count);
                            if (Constant.CartValues.containsKey(variants.getId())) {
                                Constant.CartValues.replace(variants.getId(), "" + count);
                            } else {
                                Constant.CartValues.put(variants.getId(), "" + count);
                            }
                            ApiConfig.AddMultipleProductInCart(session, activity, Constant.CartValues);
                        }
                    } else {
                        holder.tvQuantity.setText("" + count);
                        databaseHelper.AddToCart(variants.getId(), variants.getProduct_id(), "" + count);
                        databaseHelper.getTotalItemOfCart(activity);
                    }
                } else {
                    Toast.makeText(activity, activity.getString(R.string.limit_alert), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(activity, activity.getString(R.string.stock_limit), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(activity, activity.getString(R.string.user_deactivate_msg), Toast.LENGTH_SHORT).show();
        }
    }


    @NonNull
    @Override
    public ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(itemResource, parent, false);
        return new ItemHolder(view);
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ItemHolder extends RecyclerView.ViewHolder {

        public final ImageView imgProduct, btnAddQty, btnMinusQty;
        public final TextView tvTitle, tvPrice, tvQuantity, tvDPrice;
        public final RelativeLayout relativeLayout, lytQuantity;
        public final TextView btnAddToCart;
        public final TextView tvStatus;

        public ItemHolder(View itemView) {
            super(itemView);
            imgProduct = itemView.findViewById(R.id.imgProduct);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvDPrice = itemView.findViewById(R.id.tvDPrice);
            relativeLayout = itemView.findViewById(R.id.play_layout);
            lytQuantity = itemView.findViewById(R.id.lytQuantity);
            btnAddQty = itemView.findViewById(R.id.btnAddQty);
            btnMinusQty = itemView.findViewById(R.id.btnMinusQty);
            tvQuantity = itemView.findViewById(R.id.tvQuantity);
            btnAddToCart = itemView.findViewById(R.id.btnAddToCart);
            tvStatus = itemView.findViewById(R.id.tvStatus);

        }


    }
}
