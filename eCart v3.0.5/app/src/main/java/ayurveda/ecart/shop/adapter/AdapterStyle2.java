package ayurveda.ecart.shop.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import ayurveda.ecart.shop.R;
import ayurveda.ecart.shop.fragment.ProductDetailFragment;
import ayurveda.ecart.shop.helper.ApiConfig;
import ayurveda.ecart.shop.helper.Constant;
import ayurveda.ecart.shop.helper.DatabaseHelper;
import ayurveda.ecart.shop.helper.Session;
import ayurveda.ecart.shop.model.Product;
import ayurveda.ecart.shop.model.Variants;

/**
 * Created by shree1 on 3/16/2017.
 */

public class AdapterStyle2 extends RecyclerView.Adapter<AdapterStyle2.ItemHolder> {

    public final ArrayList<Product> productList;
    public final Activity activity;
    Session session;
    boolean isLogin;
    DatabaseHelper databaseHelper;
    HashMap<String, Long> hashMap;
    long available_stock;

    public AdapterStyle2(Activity activity, ArrayList<Product> productList, HashMap<String, Long> hashMap) {
        this.activity = activity;
        this.session = new Session(activity);
        this.databaseHelper = new DatabaseHelper(activity);
        isLogin = session.getBoolean(Constant.IS_USER_LOGIN);
        this.productList = productList;
        this.hashMap = hashMap;
        this.available_stock = 0;
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ItemHolder holder, final int position) {

        if (productList.size() > 0) {
            Product product = productList.get(0);
            Variants variant = product.getVariants().get(0);
            String maxCartCont;

            if (product.getTotal_allowed_quantity() == null || product.getTotal_allowed_quantity().equals("") || product.getTotal_allowed_quantity().equals("0")) {
                maxCartCont = session.getData(Constant.max_cart_items_count);
            } else {
                maxCartCont = product.getTotal_allowed_quantity();
            }

            if (variant.getServe_for().equalsIgnoreCase(Constant.SOLD_OUT_TEXT)) {
                holder.tvStatus2_1.setVisibility(View.VISIBLE);
                holder.lytQuantity2_1.setVisibility(View.GONE);
            } else {
                holder.tvStatus2_1.setVisibility(View.GONE);
                holder.lytQuantity2_1.setVisibility(View.VISIBLE);
            }

            double price, oPrice;
            String taxPercentage = "0";
            try {
                taxPercentage = (Double.parseDouble(productList.get(0).getTax_percentage()) > 0 ? productList.get(0).getTax_percentage() : "0");
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (variant.getDiscounted_price().equals("0") || variant.getDiscounted_price().equals("")) {
                holder.tvSubStyle2_1_.setVisibility(View.GONE);
                price = ((Float.parseFloat(variant.getPrice()) + ((Float.parseFloat(variant.getPrice()) * Float.parseFloat(taxPercentage)) / 100)));
            } else {
                price = ((Float.parseFloat(variant.getDiscounted_price()) + ((Float.parseFloat(variant.getDiscounted_price()) * Float.parseFloat(taxPercentage)) / 100)));
                oPrice = (Float.parseFloat(variant.getPrice()) + ((Float.parseFloat(variant.getPrice()) * Float.parseFloat(taxPercentage)) / 100));

                holder.tvSubStyle2_1_.setPaintFlags(holder.tvSubStyle2_1_.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.tvSubStyle2_1_.setText(new Session(activity).getData(Constant.CURRENCY) + ApiConfig.StringFormat("" + oPrice));

                holder.tvSubStyle2_1_.setVisibility(View.VISIBLE);
            }

            holder.tvSubStyle2_1.setText(new Session(activity).getData(Constant.CURRENCY) + ApiConfig.StringFormat("" + price));

            holder.tvStyle2_1.setText(product.getName());

            Picasso.get().
                    load((product.getImage() == null || product.getImage().equals("")) ? "-" : product.getImage())
                    .fit()
                    .centerInside()
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(holder.imgProduct2_1);

            holder.layoutStyle2_1.setOnClickListener(view -> {
                AppCompatActivity activity1 = (AppCompatActivity) activity;
                Fragment fragment = new ProductDetailFragment();
                final Bundle bundle = new Bundle();
                bundle.putString(Constant.FROM, "section");
                bundle.putInt(Constant.VARIANT_POSITION, 0);
                bundle.putString(Constant.ID, product.getId());
                fragment.setArguments(bundle);
                activity1.getSupportFragmentManager().beginTransaction().add(R.id.container, fragment).addToBackStack(null).commit();
            });

            if (isLogin) {
                holder.tvQuantity2_1.setText(variant.getCart_count());
            } else {
                holder.tvQuantity2_1.setText(databaseHelper.CheckCartItemExist(variant.getId(), variant.getProduct_id()));
            }

            holder.btnAddToCart2_1.setVisibility(holder.tvQuantity2_1.getText().equals("0") ? View.VISIBLE : View.GONE);

            boolean isLoose = variant.getType().equalsIgnoreCase("loose");

            if (isLoose) {
                holder.btnMinusQty2_1.setOnClickListener(view -> removeLooseItemFromCartClickEvent(variant, holder.tvQuantity2_1, holder.btnAddToCart2_1));

                holder.btnAddQty2_1.setOnClickListener(view -> addLooseItemToCartClickEvent(variant, maxCartCont, holder.tvQuantity2_1, holder.btnAddToCart2_1));

                holder.btnAddToCart2_1.setOnClickListener(v -> addLooseItemToCartClickEvent(variant, maxCartCont, holder.tvQuantity2_1, holder.btnAddToCart2_1));
            } else {
                holder.btnMinusQty2_1.setOnClickListener(view -> removeFromCartClickEvent(variant, maxCartCont, holder.tvQuantity2_1, holder.btnAddToCart2_1));

                holder.btnAddQty2_1.setOnClickListener(view -> addToCartClickEvent(variant, maxCartCont, holder.tvQuantity2_1, holder.btnAddToCart2_1));

                holder.btnAddToCart2_1.setOnClickListener(v -> addToCartClickEvent(variant, maxCartCont, holder.tvQuantity2_1, holder.btnAddToCart2_1));
            }
        }

        if (productList.size() > 1) {
            Product product = productList.get(1);
            Variants variant = product.getVariants().get(0);
            String maxCartCont;

            if (product.getTotal_allowed_quantity() == null || product.getTotal_allowed_quantity().equals("") || product.getTotal_allowed_quantity().equals("0")) {
                maxCartCont = session.getData(Constant.max_cart_items_count);
            } else {
                maxCartCont = product.getTotal_allowed_quantity();
            }

            double price, oPrice;
            String taxPercentage = "0";
            try {
                taxPercentage = (Double.parseDouble(productList.get(1).getTax_percentage()) > 0 ? productList.get(1).getTax_percentage() : "0");
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (variant.getServe_for().equalsIgnoreCase(Constant.SOLD_OUT_TEXT)) {
                holder.tvStatus2_2.setVisibility(View.VISIBLE);
                holder.lytQuantity2_2.setVisibility(View.GONE);
            } else {
                holder.tvStatus2_2.setVisibility(View.GONE);
                holder.lytQuantity2_2.setVisibility(View.VISIBLE);
            }

            if (variant.getDiscounted_price().equals("0") || variant.getDiscounted_price().equals("")) {
                holder.tvSubStyle2_2_.setVisibility(View.GONE);
                price = ((Float.parseFloat(variant.getPrice()) + ((Float.parseFloat(variant.getPrice()) * Float.parseFloat(taxPercentage)) / 100)));
            } else {
                price = ((Float.parseFloat(variant.getDiscounted_price()) + ((Float.parseFloat(variant.getDiscounted_price()) * Float.parseFloat(taxPercentage)) / 100)));
                oPrice = (Float.parseFloat(variant.getPrice()) + ((Float.parseFloat(variant.getPrice()) * Float.parseFloat(taxPercentage)) / 100));

                holder.tvSubStyle2_2_.setPaintFlags(holder.tvSubStyle2_2_.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.tvSubStyle2_2_.setText(new Session(activity).getData(Constant.CURRENCY) + ApiConfig.StringFormat("" + oPrice));

                holder.tvSubStyle2_2_.setVisibility(View.VISIBLE);
            }

            holder.tvStyle2_2.setText(product.getName());

            holder.tvSubStyle2_2.setText(new Session(activity).getData(Constant.CURRENCY) + ApiConfig.StringFormat("" + price));

            Picasso.get().
                    load((product.getImage() == null || product.getImage().equals("")) ? "-" : product.getImage())
                    .fit()
                    .centerInside()
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(holder.imgProduct2_2);

            holder.layoutStyle2_2.setOnClickListener(view -> {
                AppCompatActivity activity1 = (AppCompatActivity) activity;
                Fragment fragment = new ProductDetailFragment();
                final Bundle bundle = new Bundle();
                bundle.putString(Constant.FROM, "section");
                bundle.putInt(Constant.VARIANT_POSITION, 0);
                bundle.putString(Constant.ID, product.getId());
                fragment.setArguments(bundle);
                activity1.getSupportFragmentManager().beginTransaction().add(R.id.container, fragment).addToBackStack(null).commit();
            });


            if (isLogin) {
                holder.tvQuantity2_2.setText(variant.getCart_count());
            } else {
                holder.tvQuantity2_2.setText(databaseHelper.CheckCartItemExist(variant.getId(), variant.getProduct_id()));
            }

            holder.btnAddToCart2_2.setVisibility(holder.tvQuantity2_2.getText().equals("0") ? View.VISIBLE : View.GONE);

            boolean isLoose = variant.getType().equalsIgnoreCase("loose");

            if (isLoose) {
                holder.btnMinusQty2_2.setOnClickListener(view -> removeLooseItemFromCartClickEvent(variant, holder.tvQuantity2_2, holder.btnAddToCart2_2));

                holder.btnAddQty2_2.setOnClickListener(view -> addLooseItemToCartClickEvent(variant, maxCartCont, holder.tvQuantity2_2, holder.btnAddToCart2_2));

                holder.btnAddToCart2_2.setOnClickListener(v -> addLooseItemToCartClickEvent(variant, maxCartCont, holder.tvQuantity2_2, holder.btnAddToCart2_2));
            } else {
                holder.btnMinusQty2_2.setOnClickListener(view -> removeFromCartClickEvent(variant, maxCartCont, holder.tvQuantity2_2, holder.btnAddToCart2_2));

                holder.btnAddQty2_2.setOnClickListener(view -> addToCartClickEvent(variant, maxCartCont, holder.tvQuantity2_2, holder.btnAddToCart2_2));

                holder.btnAddToCart2_2.setOnClickListener(v -> addToCartClickEvent(variant, maxCartCont, holder.tvQuantity2_2, holder.btnAddToCart2_2));
            }
        }

        if (productList.size() > 2) {

            Product product = productList.get(2);
            Variants variant = product.getVariants().get(0);
            String maxCartCont;

            if (product.getTotal_allowed_quantity() == null || product.getTotal_allowed_quantity().equals("") || product.getTotal_allowed_quantity().equals("0")) {
                maxCartCont = session.getData(Constant.max_cart_items_count);
            } else {
                maxCartCont = product.getTotal_allowed_quantity();
            }

            holder.tvStyle2_3.setText(product.getName());

            double price, oPrice;
            String taxPercentage = "0";
            try {
                taxPercentage = (Double.parseDouble(productList.get(2).getTax_percentage()) > 0 ? productList.get(2).getTax_percentage() : "0");
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (variant.getServe_for().equalsIgnoreCase(Constant.SOLD_OUT_TEXT)) {
                holder.tvStatus2_3.setVisibility(View.VISIBLE);
                holder.lytQuantity2_3.setVisibility(View.GONE);
            } else {
                holder.tvStatus2_3.setVisibility(View.GONE);
                holder.lytQuantity2_3.setVisibility(View.VISIBLE);
            }

            if (variant.getDiscounted_price().equals("0") || variant.getDiscounted_price().equals("")) {
                holder.tvSubStyle2_3_.setVisibility(View.GONE);
                price = ((Float.parseFloat(variant.getPrice()) + ((Float.parseFloat(variant.getPrice()) * Float.parseFloat(taxPercentage)) / 100)));
            } else {
                price = ((Float.parseFloat(variant.getDiscounted_price()) + ((Float.parseFloat(variant.getDiscounted_price()) * Float.parseFloat(taxPercentage)) / 100)));
                oPrice = (Float.parseFloat(variant.getPrice()) + ((Float.parseFloat(variant.getPrice()) * Float.parseFloat(taxPercentage)) / 100));

                holder.tvSubStyle2_3_.setPaintFlags(holder.tvSubStyle2_3_.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.tvSubStyle2_3_.setText(new Session(activity).getData(Constant.CURRENCY) + ApiConfig.StringFormat("" + oPrice));

                holder.tvSubStyle2_3_.setVisibility(View.VISIBLE);
            }

            holder.tvSubStyle2_3.setText(new Session(activity).getData(Constant.CURRENCY) + ApiConfig.StringFormat("" + price));


            Picasso.get().
                    load((product.getImage() == null || product.getImage().equals("")) ? "-" : product.getImage())
                    .fit()
                    .centerInside()
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(holder.imgProduct2_3);

            holder.layoutStyle2_3.setOnClickListener(view -> {
                AppCompatActivity activity1 = (AppCompatActivity) activity;
                Fragment fragment = new ProductDetailFragment();
                final Bundle bundle = new Bundle();
                bundle.putString(Constant.FROM, "section");
                bundle.putInt(Constant.VARIANT_POSITION, 0);
                bundle.putString(Constant.ID, product.getId());
                fragment.setArguments(bundle);
                activity1.getSupportFragmentManager().beginTransaction().add(R.id.container, fragment).addToBackStack(null).commit();
            });

            if (isLogin) {
                holder.tvQuantity2_3.setText(variant.getCart_count());
            } else {
                holder.tvQuantity2_3.setText(databaseHelper.CheckCartItemExist(variant.getId(), variant.getProduct_id()));
            }

            holder.btnAddToCart2_3.setVisibility(holder.tvQuantity2_3.getText().equals("0") ? View.VISIBLE : View.GONE);


            boolean isLoose = variant.getType().equalsIgnoreCase("loose");

            if (isLoose) {
                holder.btnMinusQty2_3.setOnClickListener(view -> removeLooseItemFromCartClickEvent(variant, holder.tvQuantity2_3, holder.btnAddToCart2_3));

                holder.btnAddQty2_3.setOnClickListener(view -> addLooseItemToCartClickEvent(variant, maxCartCont, holder.tvQuantity2_3, holder.btnAddToCart2_3));

                holder.btnAddToCart2_3.setOnClickListener(v -> addLooseItemToCartClickEvent(variant, maxCartCont, holder.tvQuantity2_3, holder.btnAddToCart2_3));
            } else {
                holder.btnMinusQty2_3.setOnClickListener(view -> removeFromCartClickEvent(variant, maxCartCont, holder.tvQuantity2_3, holder.btnAddToCart2_3));

                holder.btnAddQty2_3.setOnClickListener(view -> addToCartClickEvent(variant, maxCartCont, holder.tvQuantity2_3, holder.btnAddToCart2_3));

                holder.btnAddToCart2_3.setOnClickListener(v -> addToCartClickEvent(variant, maxCartCont, holder.tvQuantity2_3, holder.btnAddToCart2_3));
            }
        }
    }


    public void addLooseItemToCartClickEvent(Variants variants, String maxCartCont, TextView tvQuantity, TextView btnAddToCart) {
        long unitMeasurement = (variants.getMeasurement_unit_name().equalsIgnoreCase("kg") || variants.getMeasurement_unit_name().equalsIgnoreCase("ltr")) ? 1000 : 1;
        long unit = (long) (Double.parseDouble(variants.getMeasurement()) * unitMeasurement);
        available_stock = hashMap.get(variants.getProduct_id());
        if (session.getData(Constant.STATUS).equals("1")) {
            int count = Integer.parseInt(tvQuantity.getText().toString());

            if (count <= Integer.parseInt(maxCartCont)) {
                count++;
                if (available_stock >= unit) {
                    if (count != 0) {
                        btnAddToCart.setVisibility(View.GONE);
                    }
                    if (hashMap.containsKey(variants.getProduct_id())) {
                        hashMap.replace(variants.getProduct_id(), (hashMap.get(variants.getProduct_id()) - unit));
                    } else {
                        hashMap.put(variants.getProduct_id(), unit);
                    }
                    if (isLogin) {
                        if (Constant.CartValues.containsKey(variants.getId())) {
                            Constant.CartValues.replace(variants.getId(), "" + count);
                        } else {
                            Constant.CartValues.put(variants.getId(), "" + count);
                        }
                        ApiConfig.AddMultipleProductInCart(session, activity, Constant.CartValues);
                    } else {
                        databaseHelper.AddToCart(variants.getId(), variants.getProduct_id(), "" + count);
                        databaseHelper.getTotalItemOfCart(activity);
                    }
                    if (count > 0) {
                        btnAddToCart.setVisibility(View.GONE);
                    } else {
                        btnAddToCart.setVisibility(View.VISIBLE);
                    }
                    tvQuantity.setText("" + count);
                } else {
                    Toast.makeText(activity, activity.getString(R.string.stock_limit), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(activity, activity.getString(R.string.limit_alert), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(activity, activity.getString(R.string.user_deactivate_msg), Toast.LENGTH_SHORT).show();
        }
    }

    public void removeLooseItemFromCartClickEvent(Variants variants, TextView tvQuantity, TextView btnAddToCart) {

        long unitMeasurement = (variants.getMeasurement_unit_name().equalsIgnoreCase("kg") || variants.getMeasurement_unit_name().equalsIgnoreCase("ltr")) ? 1000 : 1;
        long unit = (long) Double.parseDouble(variants.getMeasurement()) * unitMeasurement;

        if (session.getData(Constant.STATUS).equals("1")) {
            int count = Integer.parseInt(tvQuantity.getText().toString());
            count--;
            available_stock += unit;
            if (count == 0) {
                btnAddToCart.setVisibility(View.VISIBLE);
            }
            if (isLogin) {
                if (count <= 0) {
                    tvQuantity.setText("" + count);
                    if (Constant.CartValues.containsKey(variants.getId())) {
                        Constant.CartValues.replace(variants.getId(), "" + count);
                    } else {
                        Constant.CartValues.put(variants.getId(), "" + count);
                    }
                    ApiConfig.AddMultipleProductInCart(session, activity, Constant.CartValues);
                }
            } else {
                tvQuantity.setText("" + count);
                databaseHelper.AddToCart(variants.getId(), variants.getProduct_id(), "" + count);
                databaseHelper.getTotalItemOfCart(activity);
            }

        } else {
            Toast.makeText(activity, activity.getString(R.string.user_deactivate_msg), Toast.LENGTH_SHORT).show();
        }
    }

    public void addToCartClickEvent(Variants variants, String maxCartCont, TextView tvQuantity, TextView btnAddToCart) {
        if (session.getData(Constant.STATUS).equals("1")) {
            int count = Integer.parseInt(tvQuantity.getText().toString());
            if (count < Float.parseFloat(variants.getStock())) {
                if (count < Integer.parseInt(maxCartCont)) {
                    count++;
                    if (count != 0) {
                        btnAddToCart.setVisibility(View.GONE);
                    }
                    tvQuantity.setText("" + count);
                    if (isLogin) {
                        if (Constant.CartValues.containsKey(variants.getId())) {
                            Constant.CartValues.replace(variants.getId(), "" + count);
                        } else {
                            Constant.CartValues.put(variants.getId(), "" + count);
                        }
                        ApiConfig.AddMultipleProductInCart(session, activity, Constant.CartValues);
                    } else {
                        tvQuantity.setText("" + count);
                        databaseHelper.AddToCart(variants.getId(), variants.getProduct_id(), "" + count);
                        databaseHelper.getTotalItemOfCart(activity);
                    }
                } else {
                    Toast.makeText(activity, activity.getString(R.string.limit_alert), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(activity, activity.getString(R.string.stock_limit), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(activity, activity.getString(R.string.user_deactivate_msg), Toast.LENGTH_SHORT).show();
        }
    }

    public void removeFromCartClickEvent(Variants variants, String maxCartCont, TextView tvQuantity, TextView btnAddToCart) {
        if (session.getData(Constant.STATUS).equals("1")) {
            int count = Integer.parseInt(tvQuantity.getText().toString());
            if (count <= Float.parseFloat(variants.getStock())) {
                if (count <= Integer.parseInt(maxCartCont)) {
                    count--;
                    if (count == 0) {
                        btnAddToCart.setVisibility(View.VISIBLE);
                    }
                    tvQuantity.setText("" + count);
                    if (isLogin) {
                        if (count <= 0) {
                            tvQuantity.setText("" + count);
                            if (Constant.CartValues.containsKey(variants.getId())) {
                                Constant.CartValues.replace(variants.getId(), "" + count);
                            } else {
                                Constant.CartValues.put(variants.getId(), "" + count);
                            }
                            ApiConfig.AddMultipleProductInCart(session, activity, Constant.CartValues);
                        }
                    } else {
                        tvQuantity.setText("" + count);
                        databaseHelper.AddToCart(variants.getId(), variants.getProduct_id(), "" + count);
                        databaseHelper.getTotalItemOfCart(activity);
                    }
                } else {
                    Toast.makeText(activity, activity.getString(R.string.limit_alert), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(activity, activity.getString(R.string.stock_limit), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(activity, activity.getString(R.string.user_deactivate_msg), Toast.LENGTH_SHORT).show();
        }
    }

    @NonNull
    @Override
    public ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lyt_style_2, parent, false);
        return new ItemHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    public static class ItemHolder extends RecyclerView.ViewHolder {

        public final ImageView imgProduct2_1;
        public final ImageView imgProduct2_2;
        public final ImageView imgProduct2_3;

        public final TextView tvStyle2_1;
        public final TextView tvStyle2_2;
        public final TextView tvStyle2_3;

        public final TextView tvSubStyle2_1;
        public final TextView tvSubStyle2_1_;
        public final TextView tvSubStyle2_2;
        public final TextView tvSubStyle2_2_;
        public final TextView tvSubStyle2_3;
        public final TextView tvSubStyle2_3_;

        public final CardView layoutStyle2_1;
        public final CardView layoutStyle2_2;
        public final CardView layoutStyle2_3;

        public final TextView tvStatus2_1;
        public final TextView tvStatus2_2;
        public final TextView tvStatus2_3;

        public final RelativeLayout lytQuantity2_1;
        public final RelativeLayout lytQuantity2_2;
        public final RelativeLayout lytQuantity2_3;

        public final ImageView btnMinusQty2_1;
        public final ImageView btnMinusQty2_2;
        public final ImageView btnMinusQty2_3;

        public final TextView tvQuantity2_1;
        public final TextView tvQuantity2_2;
        public final TextView tvQuantity2_3;

        public final ImageView btnAddQty2_1;
        public final ImageView btnAddQty2_2;
        public final ImageView btnAddQty2_3;

        public final TextView btnAddToCart2_1;
        public final TextView btnAddToCart2_2;
        public final TextView btnAddToCart2_3;


        public ItemHolder(View itemView) {
            super(itemView);
            imgProduct2_1 = itemView.findViewById(R.id.imgProduct2_1);
            imgProduct2_2 = itemView.findViewById(R.id.imgProduct2_2);
            imgProduct2_3 = itemView.findViewById(R.id.imgProduct2_3);

            tvStyle2_1 = itemView.findViewById(R.id.tvStyle2_1);
            tvStyle2_2 = itemView.findViewById(R.id.tvStyle2_2);
            tvStyle2_3 = itemView.findViewById(R.id.tvStyle2_3);

            tvSubStyle2_1 = itemView.findViewById(R.id.tvSubStyle2_1);
            tvSubStyle2_1_ = itemView.findViewById(R.id.tvSubStyle2_1_);
            tvSubStyle2_2 = itemView.findViewById(R.id.tvSubStyle2_2);
            tvSubStyle2_2_ = itemView.findViewById(R.id.tvSubStyle2_2_);
            tvSubStyle2_3 = itemView.findViewById(R.id.tvSubStyle2_3);
            tvSubStyle2_3_ = itemView.findViewById(R.id.tvSubStyle2_3_);

            layoutStyle2_1 = itemView.findViewById(R.id.layoutStyle2_1);
            layoutStyle2_2 = itemView.findViewById(R.id.layoutStyle2_2);
            layoutStyle2_3 = itemView.findViewById(R.id.layoutStyle2_3);

            tvStatus2_1 = itemView.findViewById(R.id.tvStatus2_1);
            tvStatus2_2 = itemView.findViewById(R.id.tvStatus2_2);
            tvStatus2_3 = itemView.findViewById(R.id.tvStatus2_3);

            lytQuantity2_1 = itemView.findViewById(R.id.lytQuantity2_1);
            lytQuantity2_2 = itemView.findViewById(R.id.lytQuantity2_2);
            lytQuantity2_3 = itemView.findViewById(R.id.lytQuantity2_3);

            btnMinusQty2_1 = itemView.findViewById(R.id.btnMinusQty2_1);
            btnMinusQty2_2 = itemView.findViewById(R.id.btnMinusQty2_2);
            btnMinusQty2_3 = itemView.findViewById(R.id.btnMinusQty2_3);

            tvQuantity2_1 = itemView.findViewById(R.id.tvQuantity2_1);
            tvQuantity2_2 = itemView.findViewById(R.id.tvQuantity2_2);
            tvQuantity2_3 = itemView.findViewById(R.id.tvQuantity2_3);

            btnAddQty2_1 = itemView.findViewById(R.id.btnAddQty2_1);
            btnAddQty2_2 = itemView.findViewById(R.id.btnAddQty2_2);
            btnAddQty2_3 = itemView.findViewById(R.id.btnAddQty2_3);

            btnAddToCart2_1 = itemView.findViewById(R.id.btnAddToCart2_1);
            btnAddToCart2_2 = itemView.findViewById(R.id.btnAddToCart2_2);
            btnAddToCart2_3 = itemView.findViewById(R.id.btnAddToCart2_3);

        }


    }
}
