package ayurveda.ecart.shop.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import ayurveda.ecart.shop.R;
import ayurveda.ecart.shop.activity.MainActivity;
import ayurveda.ecart.shop.fragment.BlogListFragment;
import ayurveda.ecart.shop.model.BlogCategory;

public class BlogCategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    // for load more
    public final int VIEW_TYPE_ITEM = 0;
    public final int VIEW_TYPE_LOADING = 1;
    final Activity activity;
    final ArrayList<BlogCategory> blogCategories;
    boolean visible;


    public BlogCategoryAdapter(Activity activity, ArrayList<BlogCategory> blogCategories) {
        this.activity = activity;
        this.blogCategories = blogCategories;
        visible = false;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, final int viewType) {
        View view;
        switch (viewType) {
            case (VIEW_TYPE_ITEM):
                view = LayoutInflater.from(activity).inflate(R.layout.lyt_category_blog_list, parent, false);
                return new ItemHolder(view);
            case (VIEW_TYPE_LOADING):
                view = LayoutInflater.from(activity).inflate(R.layout.item_progressbar, parent, false);
                return new ViewHolderLoading(view);
            default:
                throw new IllegalArgumentException("unexpected viewType: " + viewType);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderParent, final int position) {

        if (holderParent instanceof ItemHolder) {
            final ItemHolder holder = (ItemHolder) holderParent;
            final BlogCategory blogCategory = blogCategories.get(position);

            holder.tvTitle.setText(blogCategory.getName());

            Picasso.get()
                    .load(blogCategory.getImage())
                    .fit()
                    .centerInside()
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(holder.imgBlogCategory);

            holder.lytMain.setOnClickListener(v -> {
                Fragment fragment = new BlogListFragment();
                Bundle bundle = new Bundle();
                bundle.putString("title", blogCategory.getName());
                bundle.putString("id", blogCategory.getId());
                fragment.setArguments(bundle);
                MainActivity.fm.beginTransaction().add(R.id.container, fragment).addToBackStack(null).commit();
            });


        } else if (holderParent instanceof ViewHolderLoading) {
            ViewHolderLoading loadingViewHolder = (ViewHolderLoading) holderParent;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return blogCategories.size();
    }

    @Override
    public int getItemViewType(int position) {
        return blogCategories.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(blogCategories.get(position).getId());
    }

    static class ViewHolderLoading extends RecyclerView.ViewHolder {
        public final ProgressBar progressBar;

        public ViewHolderLoading(View view) {
            super(view);
            progressBar = view.findViewById(R.id.itemProgressbar);
        }
    }

    static class ItemHolder extends RecyclerView.ViewHolder {

        final ImageView imgBlogCategory;
        final TextView tvTitle;
        final LinearLayout lytMain;


        public ItemHolder(@NonNull View itemView) {
            super(itemView);

            imgBlogCategory = itemView.findViewById(R.id.imgBlogCategory);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            lytMain = itemView.findViewById(R.id.lytMain);
        }
    }
}
