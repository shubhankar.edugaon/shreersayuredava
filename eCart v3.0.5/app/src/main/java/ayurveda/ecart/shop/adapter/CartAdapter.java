package ayurveda.ecart.shop.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import ayurveda.ecart.shop.R;
import ayurveda.ecart.shop.fragment.CartFragment;
import ayurveda.ecart.shop.helper.ApiConfig;
import ayurveda.ecart.shop.helper.Constant;
import ayurveda.ecart.shop.helper.Session;
import ayurveda.ecart.shop.model.Cart;
import ayurveda.ecart.shop.model.CartItems;

@SuppressLint("NotifyDataSetChanged")
public class CartAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    // for load more
    public final int VIEW_TYPE_ITEM = 0;
    public final int VIEW_TYPE_LOADING = 1;
    final Activity activity;
    final Session session;
    String taxPercentage;
    long available_stock;
    ArrayList<Cart> carts;
    ArrayList<Cart> saveForLater;

    public CartAdapter(Activity activity, ArrayList<Cart> carts, ArrayList<Cart> saveForLater) {
        this.activity = activity;
        this.carts = carts;
        this.saveForLater = saveForLater;
        available_stock = 0;
        session = new Session(activity);
        taxPercentage = "0";
    }

    public void createHashMap() {
        CartFragment.hashMap.clear();
        for (Cart cart_ : carts) {
            CartItems cartItem = cart_.getItems().get(0);
            long unitMeasurement = (cartItem.getUnit().equalsIgnoreCase("kg") || cartItem.getUnit().equalsIgnoreCase("ltr")) ? 1000 : 1;
            long unit = (long) Double.parseDouble(cartItem.getMeasurement()) * unitMeasurement;

            if (!CartFragment.hashMap.containsKey(cart_.getProduct_id())) {
                CartFragment.hashMap.put(cart_.getProduct_id(), (long) (Double.parseDouble(cartItem.getStock()) * ((cartItem.getStock_unit_name().equalsIgnoreCase("kg") || cartItem.getStock_unit_name().equalsIgnoreCase("ltr")) ? 1000 : 1) - (unit * Double.parseDouble(cart_.getQty()))));
            } else {
                long qty = (long) (unit * Double.parseDouble(cart_.getQty()));
                long available_stock = CartFragment.hashMap.get(cart_.getProduct_id()) - qty;
                CartFragment.hashMap.replace(cart_.getProduct_id(), available_stock);
            }
        }
    }

    public void add(Cart cart) {
        Constant.FLOAT_TOTAL_AMOUNT = 0;

        carts.add(cart);

        for (Cart cart_ : Constant.countList) {
            if (cart_.equals(cart))
                cart_.setQty(cart.getQty());
        }

        createHashMap();

        CartFragment.variantIdList.add(cart.getProduct_variant_id());
        CartFragment.qtyList.add(cart.getQty());

        CartFragment.SetData(activity);
        CartFragment.values.put(cart.getProduct_variant_id(), cart.getQty());

        if (carts.size() != 0) {
            CartFragment.lytTotal.setVisibility(View.VISIBLE);
        } else {
            CartFragment.lytTotal.setVisibility(View.GONE);
        }

        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        Constant.FLOAT_TOTAL_AMOUNT = 0;
        Cart cart = carts.get(position);
        CartFragment.qtyList.remove(CartFragment.variantIdList.indexOf(cart.getProduct_variant_id()));
        CartFragment.variantIdList.remove(cart.getProduct_variant_id());

        int index = 0;
        for (Cart cart_ : Constant.countList) {
            if (cart_.getProduct_id().equals(cart.getProduct_id()) && cart_.getProduct_variant_id().equals(cart.getProduct_variant_id()))
                Constant.countList.get(index).setQty("0");
            index++;
        }

        CartFragment.SetData(activity);
        if (CartFragment.values.containsKey(cart.getProduct_variant_id())) {
            CartFragment.values.replace(cart.getProduct_variant_id(), "0");
        } else {
            CartFragment.values.put(cart.getProduct_variant_id(), "0");
        }

        carts.remove(cart);

        createHashMap();

        CartFragment.isSoldOut = false;
        Constant.TOTAL_CART_ITEM = getItemCount();
        CartFragment.SetData(activity);
        activity.invalidateOptionsMenu();
        if (getItemCount() == 0 && saveForLater.size() == 0) {
            CartFragment.lytEmpty.setVisibility(View.VISIBLE);
            CartFragment.lytTotal.setVisibility(View.GONE);
        } else {
            CartFragment.lytEmpty.setVisibility(View.GONE);
            CartFragment.lytTotal.setVisibility(View.VISIBLE);
        }
        notifyDataSetChanged();
        showUndoSnackBar(cart);
    }

    @SuppressLint("SetTextI18n")
    public void moveItem(int position) {
        try {
            Constant.FLOAT_TOTAL_AMOUNT = 0;

            Cart cart = carts.get(position);

            CartFragment.qtyList.remove(CartFragment.variantIdList.indexOf(cart.getProduct_variant_id()));
            CartFragment.variantIdList.remove(cart.getProduct_variant_id());

            CartFragment.SetData(activity);

            carts.remove(cart);

            createHashMap();

            saveForLater.add(cart);

            CartFragment.saveForLaterAdapter.notifyDataSetChanged();

            if (CartFragment.lytSaveForLater.getVisibility() == View.GONE)
                CartFragment.lytSaveForLater.setVisibility(View.VISIBLE);

            CartFragment.tvSaveForLaterTitle.setText(activity.getResources().getString(R.string.save_for_later) + " (" + saveForLater.size() + ")");

            CartFragment.saveForLaterValues.put(cart.getProduct_variant_id(), cart.getQty());

            Constant.TOTAL_CART_ITEM = getItemCount();

            CartFragment.SetData(activity);

            if (getItemCount() == 0)
                CartFragment.lytTotal.setVisibility(View.GONE);

            ApiConfig.AddMultipleProductInSaveForLater(session, activity, CartFragment.saveForLaterValues);

            notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, final int viewType) {
        View view;
        switch (viewType) {
            case (VIEW_TYPE_ITEM):
                view = LayoutInflater.from(activity).inflate(R.layout.lyt_cartlist, parent, false);
                return new ItemHolder(view);
            case (VIEW_TYPE_LOADING):
                view = LayoutInflater.from(activity).inflate(R.layout.item_progressbar, parent, false);
                return new ViewHolderLoading(view);
            default:
                throw new IllegalArgumentException("unexpected viewType: " + viewType);
        }
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NotNull RecyclerView.ViewHolder holderParent, final int position) {

        int viewType = getItemViewType(position);
        switch (viewType) {
            case VIEW_TYPE_ITEM:
                final ItemHolder holder = (ItemHolder) holderParent;
                final Cart cart = carts.get(position);
                CartItems cartItems = cart.getItems().get(0);
                String maxCartCont;

                if (cartItems.getTotal_allowed_quantity() == null || cartItems.getTotal_allowed_quantity().equals("") || cartItems.getTotal_allowed_quantity().equals("0")) {
                    maxCartCont = session.getData(Constant.max_cart_items_count);
                } else {
                    maxCartCont = cartItems.getTotal_allowed_quantity();
                }

                double price;
                double oPrice;
                int qty = Integer.parseInt(cart.getQty());

                try {
                    taxPercentage = (Double.parseDouble(cartItems.getTax_percentage()) > 0 ? cartItems.getTax_percentage() : "0");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Picasso.get()
                        .load((cartItems.getImage() == null || cartItems.getImage().equals("")) ? "-" : cartItems.getImage())
                        .fit()
                        .centerInside()
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder)
                        .into(holder.imgProduct);

                holder.tvDelete.setOnClickListener(v -> removeItem(position));

                holder.tvAction.setOnClickListener(v -> moveItem(position));

                holder.tvProductName.setText(cartItems.getName());

                holder.tvMeasurement.setText(cartItems.getMeasurement() + "\u0020" + cartItems.getUnit());

                if (cartItems.getServe_for().equals(Constant.SOLD_OUT_TEXT)) {
                    holder.tvStatus.setVisibility(View.VISIBLE);
                    holder.lytQuantity.setVisibility(View.GONE);
                    CartFragment.isSoldOut = true;
                } else if (Float.parseFloat(cart.getQty()) > Float.parseFloat(maxCartCont)) {
                    holder.tvStatus.setVisibility(View.VISIBLE);
                    holder.tvStatus.setText(activity.getString(R.string.low_stock_warning1) + cartItems.getStock() + activity.getString(R.string.low_stock_warning2));
                    CartFragment.isSoldOut = true;
                }

                if (cartItems.getDiscounted_price().equals("0") || cartItems.getDiscounted_price().equals("")) {
                    price = ((Float.parseFloat(cartItems.getPrice()) + ((Float.parseFloat(cartItems.getPrice()) * Float.parseFloat(taxPercentage)) / 100)));
                } else {
                    price = ((Float.parseFloat(cartItems.getDiscounted_price()) + ((Float.parseFloat(cartItems.getDiscounted_price()) * Float.parseFloat(taxPercentage)) / 100)));
                    oPrice = ((Float.parseFloat(cartItems.getPrice()) + ((Float.parseFloat(cartItems.getPrice()) * Float.parseFloat(taxPercentage)) / 100)));
                    holder.tvOriginalPrice.setPaintFlags(holder.tvOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    holder.tvOriginalPrice.setText(session.getData(Constant.currency) + ApiConfig.StringFormat("" + oPrice));
                }

                holder.tvPrice.setText(session.getData(Constant.currency) + ApiConfig.StringFormat("" + price));

                holder.tvQuantity.setText(cart.getQty());

                holder.tvTotalPrice.setText(session.getData(Constant.currency) + ApiConfig.StringFormat("" + price * qty));

                boolean isLoose = cartItems.getType().equalsIgnoreCase("loose");

                if (isLoose) {
                    holder.btnMinusQty.setOnClickListener(view -> removeLooseItemFromCartClickEvent(holder, cartItems, cart));

                    holder.btnAddQty.setOnClickListener(view -> addLooseItemToCartClickEvent(holder, cartItems, maxCartCont, cart));
                } else {
                    holder.btnMinusQty.setOnClickListener(view -> removeFromCartClickEvent(holder, cartItems, cart, maxCartCont));

                    holder.btnAddQty.setOnClickListener(view -> addToCartClickEvent(holder, cartItems, cart, maxCartCont));
                }

                Constant.FLOAT_TOTAL_AMOUNT += (price * qty);

                CartFragment.SetData(activity);

                break;
            case VIEW_TYPE_LOADING:
                ViewHolderLoading loadingViewHolder = (ViewHolderLoading) holderParent;
                loadingViewHolder.progressBar.setIndeterminate(true);
                break;
        }
    }

    public void addLooseItemToCartClickEvent(ItemHolder holder, CartItems cartItems, String maxCartCont, Cart cart) {
        available_stock = CartFragment.hashMap.get(cartItems.getProduct_id());
        long unitMeasurement = (cartItems.getUnit().equalsIgnoreCase("kg") || cartItems.getUnit().equalsIgnoreCase("ltr")) ? 1000 : 1;
        long unit = (long) Double.parseDouble(cartItems.getMeasurement()) * unitMeasurement;

        if (session.getData(Constant.STATUS).equals("1")) {
            int count = Integer.parseInt(holder.tvQuantity.getText().toString());
            count++;
            if (count <= Integer.parseInt(maxCartCont)) {
                if (available_stock >= unit) {
                    available_stock -= unit;
                    holder.tvQuantity.setText("" + count);
                    cart.setQty("" + count);
                    CartFragment.hashMap.replace(cartItems.getProduct_id(), available_stock);
                    if (Constant.CartValues.containsKey(cartItems.getId())) {
                        Constant.CartValues.replace(cartItems.getId(), "" + count);
                    } else {
                        Constant.CartValues.put(cartItems.getId(), "" + count);
                    }

                    ArrayList<Cart> countList_ = new ArrayList<>();
                    Cart cart1 = new Cart(cart.getProduct_id(), cart.getProduct_variant_id(), cart.getQty());
                    for (Cart cart_ : Constant.countList) {
                        if ((cart1.getProduct_id().equals(cart_.getProduct_id())) && (cart1.getProduct_variant_id().equals(cart_.getProduct_variant_id()))) {
                            cart_.setQty(cart1.getQty());
                        } else {
                            countList_.add(new Cart(cart1.getProduct_id(), cart1.getProduct_variant_id(), cart1.getQty()));
                        }
                    }
                    Constant.countList.addAll(countList_);

                    if (CartFragment.variantIdList.contains(cartItems.getId())) {
                        CartFragment.qtyList.set(CartFragment.variantIdList.indexOf(cart.getProduct_variant_id()), "" + count);
                    } else {
                        CartFragment.variantIdList.add(cartItems.getId());
                        CartFragment.qtyList.add(CartFragment.variantIdList.indexOf(cart.getProduct_variant_id()), "" + count);
                    }

                    ApiConfig.AddMultipleProductInCart(session, activity, Constant.CartValues);
                    Constant.FLOAT_TOTAL_AMOUNT = 0;
                    notifyDataSetChanged();
                    CartFragment.SetData(activity);


                } else {
                    Toast.makeText(activity, activity.getString(R.string.stock_limit), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(activity, activity.getString(R.string.limit_alert), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(activity, activity.getString(R.string.user_deactivate_msg), Toast.LENGTH_SHORT).show();
        }
    }

    public void removeLooseItemFromCartClickEvent(ItemHolder holder, CartItems cartItems, Cart cart) {
        available_stock = CartFragment.hashMap.get(cartItems.getProduct_id());
        long unitMeasurement = (cartItems.getUnit().equalsIgnoreCase("kg") || cartItems.getUnit().equalsIgnoreCase("ltr")) ? 1000 : 1;
        long unit = (long) Double.parseDouble(cartItems.getMeasurement()) * unitMeasurement;

        int count = Integer.parseInt(holder.tvQuantity.getText().toString());
        if (count > 1) {
            if (session.getData(Constant.STATUS).equals("1")) {
                count--;
                available_stock += unit;
                CartFragment.hashMap.replace(cartItems.getProduct_id(), available_stock);
                holder.tvQuantity.setText("" + count);
                cart.setQty("" + count);
                if (Constant.CartValues.containsKey(cartItems.getId())) {
                    Constant.CartValues.replace(cartItems.getId(), "" + count);
                } else {
                    Constant.CartValues.put(cartItems.getId(), "" + count);
                }

                ArrayList<Cart> countList_ = new ArrayList<>();
                Cart cart1 = new Cart(cart.getProduct_id(), cart.getProduct_variant_id(), cart.getQty());
                for (Cart cart_ : Constant.countList) {
                    if ((cart1.getProduct_id().equals(cart_.getProduct_id())) && (cart1.getProduct_variant_id().equals(cart_.getProduct_variant_id()))) {
                        cart_.setQty(cart1.getQty());
                    } else {
                        countList_.add(new Cart(cart1.getProduct_id(), cart1.getProduct_variant_id(), cart1.getQty()));
                    }
                }
                Constant.countList.addAll(countList_);

                CartFragment.qtyList.set(CartFragment.variantIdList.indexOf(cart.getProduct_variant_id()), "" + count);

                ApiConfig.AddMultipleProductInCart(session, activity, Constant.CartValues);
                Constant.FLOAT_TOTAL_AMOUNT = 0;
                notifyDataSetChanged();
                CartFragment.SetData(activity);
            } else {
                Toast.makeText(activity, activity.getString(R.string.user_deactivate_msg), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void addToCartClickEvent(ItemHolder holder, CartItems cartItems, Cart cart, String maxCartCont) {
        if (session.getData(Constant.STATUS).equals("1")) {
            int count = Integer.parseInt(holder.tvQuantity.getText().toString());
            count++;
            if (count < Float.parseFloat(cartItems.getStock())) {
                if (count < Integer.parseInt(maxCartCont)) {
                    holder.tvQuantity.setText("" + count);
                    cart.setQty("" + count);
                    if (Constant.CartValues.containsKey(cartItems.getId())) {
                        Constant.CartValues.replace(cartItems.getId(), "" + count);
                    } else {
                        Constant.CartValues.put(cartItems.getId(), "" + count);
                    }
                    ArrayList<Cart> countList_ = new ArrayList<>();
                    Cart cart1 = new Cart(cartItems.getProduct_id(), cartItems.getId(), cart.getQty());
                    for (Cart cart_ : Constant.countList) {
                        if ((cart1.getProduct_id().equals(cart_.getProduct_id())) && (cart1.getProduct_variant_id().equals(cart_.getProduct_variant_id()))) {
                            cart_.setQty(cart1.getQty());
                        } else {
                            countList_.add(new Cart(cart1.getProduct_id(), cart1.getProduct_variant_id(), cart1.getQty()));
                        }
                    }
                    Constant.countList.addAll(countList_);

                    if (CartFragment.variantIdList.contains(cartItems.getId())) {
                        CartFragment.qtyList.set(CartFragment.variantIdList.indexOf(cart.getProduct_variant_id()), "" + count);
                    } else {
                        CartFragment.variantIdList.add(cartItems.getId());
                        CartFragment.qtyList.add(CartFragment.variantIdList.indexOf(cart.getProduct_variant_id()), "" + count);
                    }

                    ApiConfig.AddMultipleProductInCart(session, activity, Constant.CartValues);
                    Constant.FLOAT_TOTAL_AMOUNT = 0;
                    notifyDataSetChanged();
                    CartFragment.SetData(activity);
                } else {
                    Toast.makeText(activity, activity.getString(R.string.limit_alert), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(activity, activity.getString(R.string.stock_limit), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(activity, activity.getString(R.string.user_deactivate_msg), Toast.LENGTH_SHORT).show();
        }
    }

    public void removeFromCartClickEvent(ItemHolder holder, CartItems cartItems, Cart cart, String maxCartCont) {
        int count = Integer.parseInt(holder.tvQuantity.getText().toString());
        if (count > 1) {
            if (session.getData(Constant.STATUS).equals("1")) {
                count--;
                if (count <= Float.parseFloat(cartItems.getStock())) {
                    if (count <= Integer.parseInt(maxCartCont)) {
                        holder.tvQuantity.setText("" + count);
                        cart.setQty("" + count);
                        if (Constant.CartValues.containsKey(cartItems.getId())) {
                            Constant.CartValues.replace(cartItems.getId(), "" + count);
                        } else {
                            Constant.CartValues.put(cartItems.getId(), "" + count);
                        }
                        ArrayList<Cart> countList_ = new ArrayList<>();
                        Cart cart1 = new Cart(cart.getProduct_id(), cart.getProduct_variant_id(), cart.getQty());
                        for (Cart cart_ : Constant.countList) {
                            if ((cart1.getProduct_id().equals(cart_.getProduct_id())) && (cart1.getProduct_variant_id().equals(cart_.getProduct_variant_id()))) {
                                cart_.setQty(cart1.getQty());
                            } else {
                                countList_.add(new Cart(cart1.getProduct_id(), cart1.getProduct_variant_id(), cart1.getQty()));
                            }
                        }
                        Constant.countList.addAll(countList_);

                        CartFragment.qtyList.set(CartFragment.variantIdList.indexOf(cart.getProduct_variant_id()), "" + count);
                        ApiConfig.AddMultipleProductInCart(session, activity, Constant.CartValues);

                        Constant.FLOAT_TOTAL_AMOUNT = 0;
                        notifyDataSetChanged();
                        CartFragment.SetData(activity);
                    } else {
                        Toast.makeText(activity, activity.getString(R.string.limit_alert), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(activity, activity.getString(R.string.stock_limit), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(activity, activity.getString(R.string.user_deactivate_msg), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public int getItemCount() {
        return carts.size();
    }

    @Override
    public int getItemViewType(int position) {
        return carts.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public long getItemId(int position) {
        Cart cart = carts.get(position);
        if (cart != null)
            return Integer.parseInt(cart.getId());
        else
            return position;
    }

    static class ViewHolderLoading extends RecyclerView.ViewHolder {
        public final ProgressBar progressBar;

        public ViewHolderLoading(View view) {
            super(view);
            progressBar = view.findViewById(R.id.itemProgressbar);
        }
    }

    public static class ItemHolder extends RecyclerView.ViewHolder {
        final ImageView imgProduct;
        final ImageView btnMinusQty;
        final ImageView btnAddQty;
        final TextView tvProductName;
        final TextView tvMeasurement;
        final TextView tvPrice;
        final TextView tvOriginalPrice;
        final TextView tvQuantity;
        final TextView tvTotalPrice;
        final TextView tvStatus;
        final TextView tvDelete;
        final TextView tvAction;
        final LinearLayout lytQuantity;
        final RelativeLayout lytMain;

        public ItemHolder(@NonNull View itemView) {
            super(itemView);
            imgProduct = itemView.findViewById(R.id.imgProduct);
            tvDelete = itemView.findViewById(R.id.tvDelete);
            tvAction = itemView.findViewById(R.id.tvAction);

            btnMinusQty = itemView.findViewById(R.id.btnMinusQty);
            btnAddQty = itemView.findViewById(R.id.btnAddQty);

            tvProductName = itemView.findViewById(R.id.tvProductName);
            tvMeasurement = itemView.findViewById(R.id.tvMeasurement);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvOriginalPrice = itemView.findViewById(R.id.tvOriginalPrice);
            tvQuantity = itemView.findViewById(R.id.tvQuantity);
            tvTotalPrice = itemView.findViewById(R.id.tvTotalPrice);
            tvStatus = itemView.findViewById(R.id.tvStatus);

            lytQuantity = itemView.findViewById(R.id.lytQuantity);
            lytMain = itemView.findViewById(R.id.lytMain);
        }
    }

    void showUndoSnackBar(Cart cart) {
        final Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), activity.getString(R.string.undo_message), Snackbar.LENGTH_LONG);
        snackbar.setAction(activity.getString(R.string.undo), view -> {
            snackbar.dismiss();

            add(cart);
            notifyDataSetChanged();
            CartFragment.SetData(activity);
            CartFragment.isSoldOut = false;
            Constant.TOTAL_CART_ITEM = getItemCount();
            CartFragment.SetData(activity);
            if (getItemCount() != 0) {
                CartFragment.lytTotal.setVisibility(View.VISIBLE);
                CartFragment.lytEmpty.setVisibility(View.GONE);
            }
            ApiConfig.AddMultipleProductInCart(session, activity, CartFragment.values);
            activity.invalidateOptionsMenu();
        });
        snackbar.setActionTextColor(Color.WHITE);
        View snackBarView = snackbar.getView();
        TextView textView = snackBarView.findViewById(R.id.snackbar_text);
        textView.setMaxLines(5);
        snackbar.show();
    }
}
