package ayurveda.ecart.shop.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import ayurveda.ecart.shop.R;
import ayurveda.ecart.shop.activity.MainActivity;
import ayurveda.ecart.shop.fragment.ProductDetailFragment;
import ayurveda.ecart.shop.fragment.ProductListFragment;
import ayurveda.ecart.shop.helper.ApiConfig;
import ayurveda.ecart.shop.helper.Constant;
import ayurveda.ecart.shop.helper.DatabaseHelper;
import ayurveda.ecart.shop.helper.Session;
import ayurveda.ecart.shop.helper.Utils;
import ayurveda.ecart.shop.model.FlashSale;
import ayurveda.ecart.shop.model.Product;
import ayurveda.ecart.shop.model.Variants;

/**
 * Created by shree1 on 3/16/2017.
 */

public class FlashSaleAdapter extends RecyclerView.Adapter<FlashSaleAdapter.ItemHolder> {

    public final ArrayList<Product> productList;
    public final Activity activity;
    final Session session;
    boolean isLogin;
    DatabaseHelper databaseHelper;
    long available_stock;

    public FlashSaleAdapter(Activity activity, ArrayList<Product> productList) {
        this.activity = activity;
        this.productList = productList;
        this.session = new Session(activity);
        available_stock = 0;
        this.databaseHelper = new DatabaseHelper(activity);
        isLogin = session.getBoolean(Constant.IS_USER_LOGIN);
    }

    @Override
    public int getItemCount() {
        return Math.min(productList.size(), 6);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ItemHolder holder, final int position) {
        try {
            final Product product = productList.get(position);
            holder.setIsRecyclable(false);
            if (position == 5) {
                holder.tvViewAll.setVisibility(View.VISIBLE);
                holder.lytMain_.setVisibility(View.INVISIBLE);
            } else {
                holder.tvViewAll.setVisibility(View.INVISIBLE);
                holder.lytMain_.setVisibility(View.VISIBLE);
            }

            Variants variants = product.getVariants().get(0);

            FlashSale flashSale = variants.getFlash_sales().get(0);


            Picasso.get()
                    .load((product.getImage() == null || product.getImage().equals(""))?"-":product.getImage())
                    .fit()
                    .centerInside()
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(holder.imgProduct);

            holder.productName.setText(product.getName());

            if (variants.getServe_for().equalsIgnoreCase(Constant.SOLD_OUT_TEXT)) {
                holder.tvStatus.setVisibility(View.VISIBLE);
                holder.lytQuantity.setVisibility(View.GONE);
            } else {
                holder.tvStatus.setVisibility(View.GONE);
                holder.lytQuantity.setVisibility(View.VISIBLE);
            }

            double OriginalPrice = 0, DiscountedPrice = 0;
            String taxPercentage = "0";

            try {
                taxPercentage = (Double.parseDouble(product.getTax_percentage()) > 0 ? product.getTax_percentage() : "0");
            } catch (Exception e) {
                e.printStackTrace();
            }

            Date startDate, endDate;
            long different;

            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            if (flashSale.isIs_start()) {

                startDate = df.parse(session.getData(Constant.current_date));
                endDate = df.parse(flashSale.getEnd_date());
                different = (endDate != null ? endDate.getTime() : 0) - (startDate != null ? startDate.getTime() : 0);
                long days = (different / (60 * 60 * 24 * 1000));
                if (Utils.setFormatTime(days).equalsIgnoreCase("00")) {
                    StartTimer(holder, variants, different);
                } else {
                    holder.tvTimer.setText(ApiConfig.CalculateDays(activity, days));
                }
                holder.tvTimerTitle.setText(activity.getString(R.string.ends_in));
                if (flashSale.getDiscounted_price().equals("0") || flashSale.getDiscounted_price().equals("")) {
                    holder.showDiscount.setVisibility(View.GONE);
                    holder.tvPrice.setText(session.getData(Constant.currency) + ApiConfig.StringFormat("" + OriginalPrice));
                } else {
                    holder.showDiscount.setVisibility(View.VISIBLE);
                    DiscountedPrice = ((Float.parseFloat(flashSale.getDiscounted_price()) + ((Float.parseFloat(flashSale.getDiscounted_price()) * Float.parseFloat(taxPercentage)) / 100)));
                    OriginalPrice = (Float.parseFloat(flashSale.getPrice()) + ((Float.parseFloat(flashSale.getPrice()) * Float.parseFloat(taxPercentage)) / 100));
                    holder.tvOriginalPrice.setPaintFlags(holder.tvOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    holder.tvOriginalPrice.setText(session.getData(Constant.currency) + ApiConfig.StringFormat("" + OriginalPrice));
                    holder.tvPrice.setText(session.getData(Constant.currency) + ApiConfig.StringFormat("" + DiscountedPrice));
                }

            } else {
                startDate = df.parse(session.getData(Constant.current_date));
                endDate = df.parse(flashSale.getStart_date());
                different = (endDate != null ? endDate.getTime() : 0) - (startDate != null ? startDate.getTime() : 0);
                long days = (different / (60 * 60 * 24 * 1000));

                if (Utils.setFormatTime(days).equalsIgnoreCase("00")) {
                    StartTimer(holder, variants, different);
                } else {
                    holder.tvTimer.setText(ApiConfig.CalculateDays(activity, days));
                }
                holder.tvTimerTitle.setText(activity.getString(R.string.starts_in));
                holder.tvTimer.setText(ApiConfig.CalculateDays(activity, Math.abs(days)));
                if (variants.getDiscounted_price().equals("0") || variants.getDiscounted_price().equals("")) {
                    holder.showDiscount.setVisibility(View.GONE);
                    holder.tvPrice.setText(session.getData(Constant.currency) + ApiConfig.StringFormat("" + OriginalPrice));
                } else {
                    holder.showDiscount.setVisibility(View.VISIBLE);
                    DiscountedPrice = ((Float.parseFloat(variants.getDiscounted_price()) + ((Float.parseFloat(variants.getDiscounted_price()) * Float.parseFloat(taxPercentage)) / 100)));
                    OriginalPrice = (Float.parseFloat(variants.getPrice()) + ((Float.parseFloat(variants.getPrice()) * Float.parseFloat(taxPercentage)) / 100));
                    holder.tvOriginalPrice.setPaintFlags(holder.tvOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    holder.tvOriginalPrice.setText(session.getData(Constant.currency) + ApiConfig.StringFormat("" + OriginalPrice));
                    holder.tvPrice.setText(session.getData(Constant.currency) + ApiConfig.StringFormat("" + DiscountedPrice));
                }
            }
            holder.showDiscount.setText("-" + ApiConfig.GetDiscount(OriginalPrice, DiscountedPrice));

            holder.lytMain_.setOnClickListener(view -> {
                if (variants.getProduct_id() != null) {
                    AppCompatActivity activity1 = (AppCompatActivity) activity;
                    Fragment fragment = new ProductDetailFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString(Constant.ID, variants.getProduct_id());
                    bundle.putString(Constant.FROM, "section");
                    bundle.putInt("variantsPosition", 0);
                    fragment.setArguments(bundle);
                    activity1.getSupportFragmentManager().beginTransaction().add(R.id.container, fragment).addToBackStack(null).commit();
                }
            });

            holder.tvViewAll.setOnClickListener(view -> {
                Fragment fragment = new ProductListFragment();
                Bundle bundle = new Bundle();
                bundle.putString(Constant.FROM, "flash_sale");
                bundle.putString(Constant.NAME, activity.getString(R.string.flash_sale));
                bundle.putString(Constant.ID, flashSale.getFlash_sales_id());
                fragment.setArguments(bundle);
                MainActivity.fm.beginTransaction().add(R.id.container, fragment).addToBackStack(null).commit();
            });

            if (isLogin) {
                holder.tvQuantity.setText(variants.getCart_count());
            } else {
                holder.tvQuantity.setText(databaseHelper.CheckCartItemExist(product.getVariants().get(0).getId(), product.getVariants().get(0).getProduct_id()));
            }

            holder.btnAddToCart.setVisibility(holder.tvQuantity.getText().equals("0") ? View.VISIBLE : View.GONE);


            String maxCartCont;

            if (product.getTotal_allowed_quantity() == null || product.getTotal_allowed_quantity().equals("") || product.getTotal_allowed_quantity().equals("0")) {
                maxCartCont = session.getData(Constant.max_cart_items_count);
            } else {
                maxCartCont = product.getTotal_allowed_quantity();
            }

            boolean isLoose = variants.getType().equalsIgnoreCase("loose");


            if (isLoose) {
                holder.btnMinusQty.setOnClickListener(view -> removeLooseItemFromCartClickEvent(holder,variants));

                holder.btnAddQty.setOnClickListener(view -> addLooseItemToCartClickEvent(holder,variants, maxCartCont));

                holder.btnAddToCart.setOnClickListener(v -> addLooseItemToCartClickEvent(holder,variants, maxCartCont));
            } else {
                holder.btnMinusQty.setOnClickListener(view -> removeFromCartClickEvent(holder,variants, maxCartCont));

                holder.btnAddQty.setOnClickListener(view -> addToCartClickEvent(holder,variants, maxCartCont));

                holder.btnAddToCart.setOnClickListener(v -> addToCartClickEvent(holder,variants, maxCartCont));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void addLooseItemToCartClickEvent(ItemHolder holder, Variants variants, String maxCartCont) {
        long unitMeasurement = (variants.getMeasurement_unit_name().equalsIgnoreCase("kg") || variants.getMeasurement_unit_name().equalsIgnoreCase("ltr")) ? 1000 : 1;
        long unit = (long) Double.parseDouble(variants.getMeasurement()) * unitMeasurement;

        if (session.getData(Constant.STATUS).equals("1")) {
            int count = Integer.parseInt(holder.tvQuantity.getText().toString());

            if (count <= Integer.parseInt(maxCartCont)) {
                count++;
                if (available_stock >= unit) {
                    available_stock -= unit;
                    if (count != 0) {
                        holder.btnAddToCart.setVisibility(View.GONE);
                    }
                    if (isLogin) {
                        if (Constant.CartValues.containsKey(variants.getId())) {
                            Constant.CartValues.replace(variants.getId(), "" + count);
                        } else {
                            Constant.CartValues.put(variants.getId(), "" + count);
                        }
                        ApiConfig.AddMultipleProductInCart(session, activity, Constant.CartValues);
                    } else {
                        databaseHelper.AddToCart(variants.getId(), variants.getProduct_id(), "" + count);
                        databaseHelper.getTotalItemOfCart(activity);
                    }
                    if (count > 0) {
                        holder.btnAddToCart.setVisibility(View.GONE);
                    } else {
                        holder.btnAddToCart.setVisibility(View.VISIBLE);
                    }
                    holder.tvQuantity.setText("" + count);
                } else {
                    Toast.makeText(activity, activity.getString(R.string.stock_limit), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(activity, activity.getString(R.string.limit_alert), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(activity, activity.getString(R.string.user_deactivate_msg), Toast.LENGTH_SHORT).show();
        }
    }

    public void removeLooseItemFromCartClickEvent(ItemHolder holder, Variants variants) {

        long unitMeasurement = (variants.getMeasurement_unit_name().equalsIgnoreCase("kg") || variants.getMeasurement_unit_name().equalsIgnoreCase("ltr")) ? 1000 : 1;
        long unit = (long) Double.parseDouble(variants.getMeasurement()) * unitMeasurement;

        if (session.getData(Constant.STATUS).equals("1")) {
            int count = Integer.parseInt(holder.tvQuantity.getText().toString());
            count--;
            if (count == 0) {
                holder.btnAddToCart.setVisibility(View.VISIBLE);
            }
            available_stock += unit;
            if (isLogin) {
                if (count <= 0) {
                    holder.tvQuantity.setText("" + count);
                    if (Constant.CartValues.containsKey(variants.getId())) {
                        Constant.CartValues.replace(variants.getId(), "" + count);
                    } else {
                        Constant.CartValues.put(variants.getId(), "" + count);
                    }
                    ApiConfig.AddMultipleProductInCart(session, activity, Constant.CartValues);
                }
            } else {
                holder.tvQuantity.setText("" + count);
                databaseHelper.AddToCart(variants.getId(), variants.getProduct_id(), "" + count);
                databaseHelper.getTotalItemOfCart(activity);
            }

        } else {
            Toast.makeText(activity, activity.getString(R.string.user_deactivate_msg), Toast.LENGTH_SHORT).show();
        }
    }

    public void addToCartClickEvent(ItemHolder holder, Variants variants, String maxCartCont) {
        if (session.getData(Constant.STATUS).equals("1")) {
            int count = Integer.parseInt(holder.tvQuantity.getText().toString());
            if (count < Float.parseFloat(variants.getStock())) {
                if (count < Integer.parseInt(maxCartCont)) {
                    count++;
                    if (count != 0) {
                        holder.btnAddToCart.setVisibility(View.GONE);
                    }
                    holder.tvQuantity.setText("" + count);
                    if (isLogin) {
                        if (Constant.CartValues.containsKey(variants.getId())) {
                            Constant.CartValues.replace(variants.getId(), "" + count);
                        } else {
                            Constant.CartValues.put(variants.getId(), "" + count);
                        }
                        ApiConfig.AddMultipleProductInCart(session, activity, Constant.CartValues);
                    } else {
                        holder.tvQuantity.setText("" + count);
                        databaseHelper.AddToCart(variants.getId(), variants.getProduct_id(), "" + count);
                        databaseHelper.getTotalItemOfCart(activity);
                    }
                } else {
                    Toast.makeText(activity, activity.getString(R.string.limit_alert), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(activity, activity.getString(R.string.stock_limit), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(activity, activity.getString(R.string.user_deactivate_msg), Toast.LENGTH_SHORT).show();
        }
    }

    public void removeFromCartClickEvent(ItemHolder holder, Variants variants, String maxCartCont) {
        if (session.getData(Constant.STATUS).equals("1")) {
            int count = Integer.parseInt(holder.tvQuantity.getText().toString());
            if (count <= Float.parseFloat(variants.getStock())) {
                if (count <= Integer.parseInt(maxCartCont)) {
                    count--;
                    if (count == 0) {
                        holder.btnAddToCart.setVisibility(View.VISIBLE);
                    }
                    holder.tvQuantity.setText("" + count);
                    if (isLogin) {
                        if (count <= 0) {
                            holder.tvQuantity.setText("" + count);
                            if (Constant.CartValues.containsKey(variants.getId())) {
                                Constant.CartValues.replace(variants.getId(), "" + count);
                            } else {
                                Constant.CartValues.put(variants.getId(), "" + count);
                            }
                            ApiConfig.AddMultipleProductInCart(session, activity, Constant.CartValues);
                        }
                    } else {
                        holder.tvQuantity.setText("" + count);
                        databaseHelper.AddToCart(variants.getId(), variants.getProduct_id(), "" + count);
                        databaseHelper.getTotalItemOfCart(activity);
                    }
                } else {
                    Toast.makeText(activity, activity.getString(R.string.limit_alert), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(activity, activity.getString(R.string.stock_limit), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(activity, activity.getString(R.string.user_deactivate_msg), Toast.LENGTH_SHORT).show();
        }
    }



    @NonNull
    @Override
    public ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lyt_flash_item_grid, parent, false);
        return new ItemHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ItemHolder extends RecyclerView.ViewHolder {
        final RelativeLayout lytMain_;
        final TextView showDiscount;
        final TextView productName;
        final TextView tvOriginalPrice;
        final TextView tvPrice;
        final TextView tvTimer;
        final TextView tvTimerTitle;
        final TextView tvViewAll;
        final ImageView imgProduct, btnAddQty, btnMinusQty;
        public final TextView tvQuantity;
        public final RelativeLayout lytQuantity;
        public final TextView btnAddToCart;
        public final TextView tvStatus;

        public ItemHolder(View itemView) {
            super(itemView);
            lytMain_ = itemView.findViewById(R.id.lytMain_);
            showDiscount = itemView.findViewById(R.id.showDiscount);
            productName = itemView.findViewById(R.id.productName);
            tvOriginalPrice = itemView.findViewById(R.id.tvOriginalPrice);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvTimer = itemView.findViewById(R.id.tvTimer);
            tvTimerTitle = itemView.findViewById(R.id.tvTimerTitle);
            imgProduct = itemView.findViewById(R.id.imgProduct);
            tvViewAll = itemView.findViewById(R.id.tvViewAll);
            btnAddQty = itemView.findViewById(R.id.btnAddQty);
            btnMinusQty = itemView.findViewById(R.id.btnMinusQty);
            tvQuantity = itemView.findViewById(R.id.tvQuantity);
            btnAddToCart = itemView.findViewById(R.id.btnAddToCart);
            tvStatus = itemView.findViewById(R.id.tvStatus);
            lytQuantity = itemView.findViewById(R.id.lytQuantity);
        }
    }

    public void StartTimer(ItemHolder holder, Variants variants, long duration) {
        try {

            new CountDownTimer(duration, 1000) {
                @Override
                public void onTick(long different) {
                    int seconds = (int) (different / 1000) % 60;
                    int minutes = (int) ((different / (1000 * 60)) % 60);
                    int hours = (int) ((different / (1000 * 60 * 60)) % 24);

                    holder.tvTimer.setText(String.format("%s:%s:%s", Utils.setFormatTime(hours), Utils.setFormatTime(minutes), Utils.setFormatTime(seconds)));

                }

                @Override
                public void onFinish() {
                    if (!variants.getFlash_sales().get(0).isIs_start()) {
                        variants.getFlash_sales().get(0).setIs_start(true);
                        variants.setIs_flash_sales("true");
                    } else {
                        variants.setIs_flash_sales("false");
                    }
                    notifyDataSetChanged();
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
