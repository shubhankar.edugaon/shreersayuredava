package ayurveda.ecart.shop.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;

import ayurveda.ecart.shop.R;
import ayurveda.ecart.shop.fragment.CartFragment;
import ayurveda.ecart.shop.helper.ApiConfig;
import ayurveda.ecart.shop.helper.Constant;
import ayurveda.ecart.shop.helper.DatabaseHelper;
import ayurveda.ecart.shop.helper.Session;
import ayurveda.ecart.shop.model.OfflineCart;
import ayurveda.ecart.shop.model.OfflineItems;


@SuppressLint({"SetTextI18n", "NotifyDataSetChanged"})
    public class OfflineCartAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        // for load more
        final int VIEW_TYPE_ITEM = 0;
        final int VIEW_TYPE_LOADING = 1;
        final Activity activity;
        final DatabaseHelper databaseHelper;
        final Session session;
        ArrayList<OfflineCart> items;
        ArrayList<OfflineCart> saveForLaterItems;
        HashMap<String, Long> hashMap;
        long available_stock;

        public OfflineCartAdapter(Activity activity, ArrayList<OfflineCart> items, ArrayList<OfflineCart> saveForLaterItems, HashMap<String, Long> hashMap) {
            this.activity = activity;
            this.items = items;
            this.hashMap = hashMap;
            available_stock = 0;
            this.saveForLaterItems = saveForLaterItems;
            databaseHelper = new DatabaseHelper(activity);
            session = new Session(activity);
        }

        public void createHashMap() {
            hashMap.clear();
            for (OfflineCart cart_ : items) {
                OfflineItems cartItem = cart_.getItem().get(0);
                long unitMeasurement = (cartItem.getUnit().equalsIgnoreCase("kg") || cartItem.getUnit().equalsIgnoreCase("ltr")) ? 1000 : 1;
                long unit = (long) Double.parseDouble(cartItem.getMeasurement()) * unitMeasurement;

                if (!hashMap.containsKey(cart_.getProduct_id())) {
                    hashMap.put(cart_.getProduct_id(), (long) (Double.parseDouble(cartItem.getStock()) * ((cartItem.getStock_unit_name().equalsIgnoreCase("kg") || cartItem.getStock_unit_name().equalsIgnoreCase("ltr")) ? 1000 : 1) - (unit * Double.parseDouble(databaseHelper.CheckCartItemExist(cart_.getId(), cart_.getProduct_id())))));
                } else {
                    long qty = (long) (unit * Double.parseDouble(databaseHelper.CheckCartItemExist(cart_.getId(), cart_.getProduct_id())));
                    long available_stock = (long) (hashMap.get(cart_.getProduct_id()) - qty);
                    hashMap.replace(cart_.getProduct_id(), available_stock);
                }
            }
        }


        public void add(OfflineCart item) {
            Constant.FLOAT_TOTAL_AMOUNT = 0;
            createHashMap();

            long unitMeasurement = (item.getItem().get(0).getType().equalsIgnoreCase("kg") || item.getItem().get(0).getType().equalsIgnoreCase("ltr")) ? 1000 : 1;
            long unit = (long) Double.parseDouble(item.getItem().get(0).getMeasurement()) * unitMeasurement;

            if (!hashMap.containsKey(item.getProduct_id())) {
                hashMap.put(item.getProduct_id(), (long) (Double.parseDouble(item.getItem().get(0).getStock()) * ((item.getItem().get(0).getStock_unit_name().equalsIgnoreCase("kg") || item.getItem().get(0).getStock_unit_name().equalsIgnoreCase("ltr")) ? 1000 : 1) - (unit * Double.parseDouble(databaseHelper.CheckCartItemExist(item.getId(), item.getProduct_id())))));
            } else {
                if (hashMap.get(item.getProduct_id()) >= unit) {
                    item.getItem().get(0).setServe_for(Constant.AVAILABLE);
                    hashMap.replace(item.getProduct_id(), (long) (Double.parseDouble(item.getItem().get(0).getStock()) * ((item.getItem().get(0).getStock_unit_name().equalsIgnoreCase("kg") || item.getItem().get(0).getStock_unit_name().equalsIgnoreCase("ltr")) ? 1000 : 1) - (unit * Double.parseDouble(databaseHelper.CheckCartItemExist(item.getId(), item.getProduct_id())))));
                } else {
                    item.getItem().get(0).setServe_for(Constant.SOLD_OUT_TEXT);
                }
            }

            items.add(item);

            notifyDataSetChanged();
        }

        public void removeItem(int position) {
            OfflineCart cart = items.get(position);
            hashMap.remove(cart.getProduct_id());
            showUndoSnackBar(cart, position);
            items.remove(cart);

            createHashMap();

            databaseHelper.RemoveFromCart(cart.getId(), cart.getProduct_id());
            Constant.FLOAT_TOTAL_AMOUNT = 0;

            databaseHelper.getTotalItemOfCart(activity);

            notifyDataSetChanged();

            CartFragment.SetData(activity);

            if (getItemCount() == 0 && saveForLaterItems.size() == 0) {
                CartFragment.lytEmpty.setVisibility(View.VISIBLE);
                CartFragment.lytTotal.setVisibility(View.GONE);
            } else {
                CartFragment.lytEmpty.setVisibility(View.GONE);
                CartFragment.lytTotal.setVisibility(View.VISIBLE);
            }
        }

        public void moveItem(int position) {
            try {
                Constant.FLOAT_TOTAL_AMOUNT = 0;
                OfflineCart cart = items.get(position);
                hashMap.remove(cart.getProduct_id());
                databaseHelper.MoveToCartOrSaveForLater(cart.getId(), cart.getProduct_id(), "cart", activity);
                items.remove(cart);

                createHashMap();

                CartFragment.offlineSaveForLaterAdapter.add(cart);

                if (saveForLaterItems.size() > 0)
                    CartFragment.lytSaveForLater.setVisibility(View.VISIBLE);

                if (getItemCount() == 0)
                    CartFragment.lytTotal.setVisibility(View.GONE);

                notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @NotNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
            View view;
            switch (viewType) {
                case (VIEW_TYPE_ITEM):
                    view = LayoutInflater.from(activity).inflate(R.layout.lyt_cartlist, parent, false);
                    return new ItemHolder(view);
                case (VIEW_TYPE_LOADING):
                    view = LayoutInflater.from(activity).inflate(R.layout.item_progressbar, parent, false);
                    return new ViewHolderLoading(view);
                default:
                    throw new IllegalArgumentException("unexpected viewType: " + viewType);
            }
        }

        @Override
        public void onBindViewHolder(@NotNull RecyclerView.ViewHolder holderParent, int position) {

            if (holderParent instanceof ItemHolder) {
                ItemHolder holder = (ItemHolder) holderParent;
                OfflineCart cart = items.get(position);
                Picasso.get()
                        .load(cart.getItem().get(0).getImage())
                        .fit()
                        .centerInside()
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder)
                        .into(holder.imgProduct);

                holder.tvProductName.setText(cart.getItem().get(0).getName());
                holder.tvMeasurement.setText(cart.getItem().get(0).getMeasurement() + "\u0020" + cart.getItem().get(0).getUnit());
                double price, oPrice;
                String taxPercentage = "0";
                try {
                    taxPercentage = (Double.parseDouble(cart.getTax_percentage()) > 0 ? cart.getTax_percentage() : "0");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                holder.tvPrice.setText(new Session(activity).getData(Constant.currency) + (cart.getDiscounted_price().equals("0") ? cart.getPrice() : cart.getDiscounted_price()));

                if (cart.getDiscounted_price().equals("0") || cart.getDiscounted_price().equals("")) {
                    price = ((Float.parseFloat(cart.getPrice()) + ((Float.parseFloat(cart.getPrice()) * Float.parseFloat(taxPercentage)) / 100)));
                } else {
                    price = ((Float.parseFloat(cart.getDiscounted_price()) + ((Float.parseFloat(cart.getDiscounted_price()) * Float.parseFloat(taxPercentage)) / 100)));
                    oPrice = ((Float.parseFloat(cart.getPrice()) + ((Float.parseFloat(cart.getPrice()) * Float.parseFloat(taxPercentage)) / 100)));
                    holder.tvOriginalPrice.setPaintFlags(holder.tvOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    holder.tvOriginalPrice.setText(session.getData(Constant.currency) + ApiConfig.StringFormat("" + oPrice));
                }

                String maxCartCont;

                if (cart.getItem().get(0).getTotal_allowed_quantity() == null || cart.getItem().get(0).getTotal_allowed_quantity().equals("") || cart.getItem().get(0).getTotal_allowed_quantity().equals("0")) {
                    maxCartCont = session.getData(Constant.max_cart_items_count);
                } else {
                    maxCartCont = cart.getItem().get(0).getTotal_allowed_quantity();
                }

                if (cart.getItem().get(0).getServe_for().equals(Constant.SOLD_OUT_TEXT)) {
                    holder.tvStatus.setVisibility(View.VISIBLE);
                    holder.lytQuantity.setVisibility(View.GONE);
                    CartFragment.isSoldOut = true;
                } else if (Float.parseFloat(cart.getItem().get(0).getCart_count()) > Float.parseFloat(maxCartCont)) {
                    holder.tvStatus.setVisibility(View.VISIBLE);
                    holder.tvStatus.setText(activity.getString(R.string.low_stock_warning1) + cart.getItem().get(0).getStock() + activity.getString(R.string.low_stock_warning2));
                    CartFragment.isSoldOut = true;
                }

                holder.tvDelete.setOnClickListener(v -> removeItem(position));

                holder.tvAction.setOnClickListener(v -> moveItem(position));

                holder.tvPrice.setText(new Session(activity).getData(Constant.currency) + ApiConfig.StringFormat("" + price));

                holder.tvProductName.setText(cart.getItem().get(0).getName());

                holder.tvMeasurement.setText(cart.getItem().get(0).getMeasurement() + "\u0020" + cart.getItem().get(0).getUnit());

                holder.tvQuantity.setText(databaseHelper.CheckCartItemExist(cart.getId(), cart.getProduct_id()));

                cart.getItem().get(0).setCart_count(databaseHelper.CheckCartItemExist(cart.getId(), cart.getProduct_id()));

                holder.tvTotalPrice.setText(session.getData(Constant.currency) + ApiConfig.StringFormat("" + price * Integer.parseInt(databaseHelper.CheckCartItemExist(cart.getId(), cart.getProduct_id()))));

                Constant.FLOAT_TOTAL_AMOUNT = Constant.FLOAT_TOTAL_AMOUNT + (price * Integer.parseInt(databaseHelper.CheckCartItemExist(cart.getId(), cart.getProduct_id())));

                CartFragment.SetData(activity);

                boolean isLoose = cart.getItem().get(0).getType().equalsIgnoreCase("loose");

                if (isLoose) {
                    holder.btnMinusQty.setOnClickListener(view -> removeLooseItemFromCartClickEvent(holder, cart.getItem().get(0)));

                    holder.btnAddQty.setOnClickListener(view -> addLooseItemToCartClickEvent(holder, cart.getItem().get(0), maxCartCont));
                } else {
                    holder.btnMinusQty.setOnClickListener(view -> removeFromCartClickEvent(holder, cart.getItem().get(0), maxCartCont));

                    holder.btnAddQty.setOnClickListener(view -> addToCartClickEvent(holder, cart.getItem().get(0), maxCartCont));
                }

                if (getItemCount() == 0) {
                    CartFragment.lytTotal.setVisibility(View.GONE);
                } else {
                    CartFragment.lytTotal.setVisibility(View.VISIBLE);
                }

            } else if (holderParent instanceof ViewHolderLoading) {
                ViewHolderLoading loadingViewHolder = (ViewHolderLoading) holderParent;
                loadingViewHolder.progressBar.setIndeterminate(true);
            }

        }

        public void addLooseItemToCartClickEvent(ItemHolder holder, OfflineItems cartItems, String maxCartCont) {
            available_stock = hashMap.get(cartItems.getProduct_id());
            long unitMeasurement = (cartItems.getUnit().equalsIgnoreCase("kg") || cartItems.getUnit().equalsIgnoreCase("ltr")) ? 1000 : 1;
            long unit = (long) Double.parseDouble(cartItems.getMeasurement()) * unitMeasurement;

            if (session.getData(Constant.STATUS).equals("1")) {
                int count = Integer.parseInt(holder.tvQuantity.getText().toString());
                count++;
                if (count <= Integer.parseInt(maxCartCont)) {
                    if (available_stock >= unit) {
                        available_stock -= unit;
                        hashMap.replace(cartItems.getProduct_id(), available_stock);
                        databaseHelper.AddToCart(cartItems.getId(), cartItems.getProduct_id(), "" + count);
                        holder.tvQuantity.setText("" + count);
                        Constant.FLOAT_TOTAL_AMOUNT = 0;
                        notifyDataSetChanged();
                        CartFragment.SetData(activity);
                    } else {
                        Toast.makeText(activity, activity.getString(R.string.stock_limit), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(activity, activity.getString(R.string.limit_alert), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(activity, activity.getString(R.string.user_deactivate_msg), Toast.LENGTH_SHORT).show();
            }
        }

        public void removeLooseItemFromCartClickEvent(ItemHolder holder, OfflineItems cartItems) {
            available_stock = hashMap.get(cartItems.getProduct_id());
            long unitMeasurement = (cartItems.getUnit().equalsIgnoreCase("kg") || cartItems.getUnit().equalsIgnoreCase("ltr")) ? 1000 : 1;
            long unit = (long) Double.parseDouble(cartItems.getMeasurement()) * unitMeasurement;
            int count = Integer.parseInt(holder.tvQuantity.getText().toString());
            if (count > 1) {
                if (session.getData(Constant.STATUS).equals("1")) {
                    count--;
                    if (count > 0) {
                        available_stock += unit;
                        hashMap.replace(cartItems.getProduct_id(), available_stock);
                        holder.tvQuantity.setText("" + count);
                        databaseHelper.AddToCart(cartItems.getId(), cartItems.getProduct_id(), "" + count);
                    }
                    Constant.FLOAT_TOTAL_AMOUNT = 0;
                    notifyDataSetChanged();
                    CartFragment.SetData(activity);
                } else {
                    Toast.makeText(activity, activity.getString(R.string.user_deactivate_msg), Toast.LENGTH_SHORT).show();
                }
            }
        }

        public void addToCartClickEvent(ItemHolder holder, OfflineItems cartItems, String maxCartCont) {
            if (session.getData(Constant.STATUS).equals("1")) {
                int count = Integer.parseInt(holder.tvQuantity.getText().toString());
                count++;
                if (count < Float.parseFloat(cartItems.getStock())) {
                    if (count < Integer.parseInt(maxCartCont)) {
                        holder.tvQuantity.setText("" + count);
                        databaseHelper.AddToCart(cartItems.getId(), cartItems.getProduct_id(), "" + count);
                        Constant.FLOAT_TOTAL_AMOUNT = 0;
                        notifyDataSetChanged();
                        CartFragment.SetData(activity);
                    } else {
                        Toast.makeText(activity, activity.getString(R.string.limit_alert), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(activity, activity.getString(R.string.stock_limit), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(activity, activity.getString(R.string.user_deactivate_msg), Toast.LENGTH_SHORT).show();
            }
        }

        public void removeFromCartClickEvent(ItemHolder holder, OfflineItems cartItems, String maxCartCont) {
            int count = Integer.parseInt(holder.tvQuantity.getText().toString());
            if (count > 1) {
                if (session.getData(Constant.STATUS).equals("1")) {
                    count--;
                    if (count <= Float.parseFloat(cartItems.getStock())) {
                        if (count <= Integer.parseInt(maxCartCont)) {
                            holder.tvQuantity.setText("" + count);
                            databaseHelper.AddToCart(cartItems.getId(), cartItems.getProduct_id(), "" + count);
                            Constant.FLOAT_TOTAL_AMOUNT = 0;
                            notifyDataSetChanged();
                            CartFragment.SetData(activity);
                        } else {
                            Toast.makeText(activity, activity.getString(R.string.limit_alert), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(activity, activity.getString(R.string.stock_limit), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(activity, activity.getString(R.string.user_deactivate_msg), Toast.LENGTH_SHORT).show();
                }
            }
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        @Override
        public int getItemViewType(int position) {
            return items.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
        }

        @Override
        public long getItemId(int position) {
            OfflineCart cart = items.get(position);
            if (cart != null)
                return Integer.parseInt(cart.getId());
            else
                return position;
        }

        static class ViewHolderLoading extends RecyclerView.ViewHolder {
            public final ProgressBar progressBar;

            public ViewHolderLoading(View view) {
                super(view);
                progressBar = view.findViewById(R.id.itemProgressbar);
            }
        }

        public static class ItemHolder extends RecyclerView.ViewHolder {
            final ImageView imgProduct;
            final ImageView btnMinusQty;
            final ImageView btnAddQty;
            final TextView tvProductName;
            final TextView tvMeasurement;
            final TextView tvPrice;
            final TextView tvOriginalPrice;
            final TextView tvQuantity;
            final TextView tvTotalPrice;
            final TextView tvDelete;
            final TextView tvAction;
            final TextView tvStatus;
            final RelativeLayout lytMain;
            final LinearLayout lytQuantity;

            public ItemHolder(@NonNull View itemView) {
                super(itemView);
                lytMain = itemView.findViewById(R.id.lytMain);

                imgProduct = itemView.findViewById(R.id.imgProduct);
                tvDelete = itemView.findViewById(R.id.tvDelete);
                tvAction = itemView.findViewById(R.id.tvAction);

                btnMinusQty = itemView.findViewById(R.id.btnMinusQty);
                btnAddQty = itemView.findViewById(R.id.btnAddQty);

                tvProductName = itemView.findViewById(R.id.tvProductName);
                tvMeasurement = itemView.findViewById(R.id.tvMeasurement);
                tvPrice = itemView.findViewById(R.id.tvPrice);
                tvOriginalPrice = itemView.findViewById(R.id.tvOriginalPrice);
                tvQuantity = itemView.findViewById(R.id.tvQuantity);
                tvTotalPrice = itemView.findViewById(R.id.tvTotalPrice);
                tvStatus = itemView.findViewById(R.id.tvStatus);
                lytQuantity = itemView.findViewById(R.id.lytQuantity);
            }
        }

        void showUndoSnackBar(OfflineCart cart, int position) {
            Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), activity.getString(R.string.undo_message), Snackbar.LENGTH_LONG);
            snackbar.setBackgroundTint(ContextCompat.getColor(activity, R.color.gray));
            snackbar.setAction(activity.getString(R.string.undo), view -> {
                snackbar.dismiss();
                databaseHelper.AddToCart(cart.getItem().get(0).getId(), cart.getItem().get(0).getProduct_id(), cart.getItem().get(0).getCart_count());

                String taxPercentage1 = "0";
                try {
                    taxPercentage1 = (Double.parseDouble(cart.getTax_percentage()) > 0 ? cart.getTax_percentage() : "0");
                } catch (Exception e) {
                    e.printStackTrace();

                }

                double price;
                if (cart.getItem().get(0).getDiscounted_price().equals("0") || cart.getItem().get(0).getDiscounted_price().equals("")) {
                    price = ((Float.parseFloat(cart.getItem().get(0).getPrice()) + ((Float.parseFloat(cart.getItem().get(0).getPrice()) * Float.parseFloat(taxPercentage1)) / 100)));
                } else {
                    price = ((Float.parseFloat(cart.getItem().get(0).getDiscounted_price()) + ((Float.parseFloat(cart.getItem().get(0).getDiscounted_price()) * Float.parseFloat(taxPercentage1)) / 100)));
                }

                add(cart);

                long unitMeasurement = (cart.getItem().get(0).getType().equalsIgnoreCase("kg") || cart.getItem().get(0).getType().equalsIgnoreCase("ltr")) ? 1000 : 1;
                long unit = (long) Double.parseDouble(cart.getItem().get(0).getMeasurement()) * unitMeasurement;

                if (!hashMap.containsKey(cart.getProduct_id())) {
                    hashMap.put(cart.getProduct_id(), (long) (Double.parseDouble(cart.getItem().get(0).getStock()) * ((cart.getItem().get(0).getStock_unit_name().equalsIgnoreCase("kg") || cart.getItem().get(0).getStock_unit_name().equalsIgnoreCase("ltr")) ? 1000 : 1) - (unit * Double.parseDouble(databaseHelper.CheckCartItemExist(cart.getId(), cart.getProduct_id())))));
                } else {
                    long qty = (long) (unit * Double.parseDouble(databaseHelper.CheckCartItemExist(cart.getId(), cart.getProduct_id())));
                    long available_stock = (long) (hashMap.get(cart.getProduct_id()) - qty);
                    hashMap.replace(cart.getProduct_id(), available_stock);
                }

                notifyDataSetChanged();
                CartFragment.isSoldOut = false;
                Constant.TOTAL_CART_ITEM = getItemCount();
                Constant.FLOAT_TOTAL_AMOUNT += (price * Integer.parseInt(cart.getItem().get(0).getCart_count()));
                CartFragment.values.put(cart.getItem().get(0).getId(), databaseHelper.CheckCartItemExist(cart.getItem().get(0).getId(), cart.getItem().get(0).getProduct_id()));
                CartFragment.SetData(activity);
                activity.invalidateOptionsMenu();

            }).setActionTextColor(Color.WHITE);

            View snackBarView = snackbar.getView();
            TextView textView = snackBarView.findViewById(R.id.snackbar_text);
            textView.setMaxLines(5);
            snackbar.show();
        }
    }
