package ayurveda.ecart.shop.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Paint;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import ayurveda.ecart.shop.R;
import ayurveda.ecart.shop.fragment.CartFragment;
import ayurveda.ecart.shop.helper.ApiConfig;
import ayurveda.ecart.shop.helper.Constant;
import ayurveda.ecart.shop.helper.DatabaseHelper;
import ayurveda.ecart.shop.helper.Session;
import ayurveda.ecart.shop.model.OfflineCart;


@SuppressLint("NotifyDataSetChanged")
public class OfflineSaveForLaterAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    // for load more
    final int VIEW_TYPE_ITEM = 0;
    final int VIEW_TYPE_LOADING = 1;
    final Activity activity;
    static DatabaseHelper databaseHelper;
    final Session session;
    ArrayList<OfflineCart> saveForLaterItems;


    public OfflineSaveForLaterAdapter(Activity activity, ArrayList<OfflineCart> saveForLaterItems) {
        this.activity = activity;
        this.saveForLaterItems = saveForLaterItems;
        databaseHelper = new DatabaseHelper(activity);
        session = new Session(activity);
    }

    public void removeItem(int position) {
        OfflineCart cart = saveForLaterItems.get(position);
        databaseHelper.RemoveFromSaveForLater(cart.getProduct_id(), cart.getId());
        saveForLaterItems.remove(cart);

        if (getItemCount() == 0)
            CartFragment.lytSaveForLater.setVisibility(View.GONE);

        CartFragment.offlineSaveForLaterAdapter.notifyDataSetChanged();
        CartFragment.offlineCartAdapter.notifyDataSetChanged();
    }

    public void moveItem(int position) {
        try {
            OfflineCart cart = saveForLaterItems.get(position);
            databaseHelper.MoveToCartOrSaveForLater(cart.getId(), cart.getProduct_id(), "save_for_later",activity);
            saveForLaterItems.remove(cart);
            CartFragment.offlineCartAdapter.add(cart);

            if (getItemCount() == 0)
                CartFragment.lytSaveForLater.setVisibility(View.GONE);

            if (CartFragment.offlineCartAdapter.getItemCount() != 0)
                CartFragment.lytTotal.setVisibility(View.VISIBLE);

            notifyDataSetChanged();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, final int viewType) {
        View view;
        switch (viewType) {
            case (VIEW_TYPE_ITEM):
                view = LayoutInflater.from(activity).inflate(R.layout.lyt_save_for_later, parent, false);
                return new ItemHolder(view);
            case (VIEW_TYPE_LOADING):
                view = LayoutInflater.from(activity).inflate(R.layout.item_progressbar, parent, false);
                return new ViewHolderLoading(view);
            default:
                throw new IllegalArgumentException("unexpected viewType: " + viewType);
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NotNull RecyclerView.ViewHolder holderParent, final int position) {

        if (holderParent instanceof ItemHolder) {
            final ItemHolder holder = (ItemHolder) holderParent;
            final OfflineCart cart = saveForLaterItems.get(position);

            Picasso.get()
                    .load(cart.getItem().get(0).getImage())
                    .fit()
                    .centerInside()
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(holder.imgProduct);

            holder.tvProductName.setText(cart.getItem().get(0).getName());

            holder.tvMeasurement.setText(cart.getItem().get(0).getMeasurement() + "\u0020" + cart.getItem().get(0).getUnit());

            double price, oPrice;
            String taxPercentage = "0";
            try {
                taxPercentage = (Double.parseDouble(cart.getTax_percentage()) > 0 ? cart.getTax_percentage() : "0");
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (cart.getDiscounted_price().equals("0") || cart.getDiscounted_price().equals("")) {
                price = ((Float.parseFloat(cart.getPrice()) + ((Float.parseFloat(cart.getPrice()) * Float.parseFloat(taxPercentage)) / 100)));
            } else {
                price = ((Float.parseFloat(cart.getDiscounted_price()) + ((Float.parseFloat(cart.getDiscounted_price()) * Float.parseFloat(taxPercentage)) / 100)));
                oPrice = ((Float.parseFloat(cart.getPrice()) + ((Float.parseFloat(cart.getPrice()) * Float.parseFloat(taxPercentage)) / 100)));
                holder.tvOriginalPrice.setPaintFlags(holder.tvOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.tvOriginalPrice.setText(session.getData(Constant.currency) + ApiConfig.StringFormat("" + oPrice));
            }

            if (!cart.getItem().get(0).getServe_for().equalsIgnoreCase("available")) {
                holder.tvStatus.setVisibility(View.VISIBLE);
            }

            holder.tvPrice.setText(new Session(activity).getData(Constant.currency) + ApiConfig.StringFormat("" + price));

            holder.tvDelete.setOnClickListener(v -> removeItem(position));

            holder.tvAction.setOnClickListener(v -> moveItem(position));


            holder.tvProductName.setText(cart.getItem().get(0).getName());
            holder.tvMeasurement.setText(cart.getItem().get(0).getMeasurement() + "\u0020" + cart.getItem().get(0).getUnit());

            if (saveForLaterItems.size() > 0) {
                CartFragment.lytSaveForLater.setVisibility(View.VISIBLE);
            } else {
                CartFragment.lytSaveForLater.setVisibility(View.GONE);
            }

        } else if (holderParent instanceof ViewHolderLoading) {
            ViewHolderLoading loadingViewHolder = (ViewHolderLoading) holderParent;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }

    }

    @Override
    public int getItemCount() {
        return saveForLaterItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return saveForLaterItems.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public long getItemId(int position) {
        OfflineCart cart = saveForLaterItems.get(position);
        if (cart != null)
            return Integer.parseInt(cart.getId());
        else
            return position;
    }

    public void add(OfflineCart cart) {
        saveForLaterItems.add(cart);
        notifyDataSetChanged();

        if (saveForLaterItems.size() != 0) {
            CartFragment.lytSaveForLater.setVisibility(View.VISIBLE);
        }
    }

    static class ViewHolderLoading extends RecyclerView.ViewHolder {
        public final ProgressBar progressBar;

        public ViewHolderLoading(View view) {
            super(view);
            progressBar = view.findViewById(R.id.itemProgressbar);
        }
    }

    public static class ItemHolder extends RecyclerView.ViewHolder {
        final ImageView imgProduct;
        final TextView tvProductName;
        final TextView tvMeasurement;
        final TextView tvPrice;
        final TextView tvOriginalPrice;
        final TextView tvDelete;
        final TextView tvAction;
        final TextView tvStatus;
        final RelativeLayout lytMain;

        public ItemHolder(@NonNull View itemView) {
            super(itemView);
            lytMain = itemView.findViewById(R.id.lytMain);

            imgProduct = itemView.findViewById(R.id.imgProduct);
            tvDelete = itemView.findViewById(R.id.tvDelete);
            tvAction = itemView.findViewById(R.id.tvAction);
            tvStatus = itemView.findViewById(R.id.tvStatus);

            tvProductName = itemView.findViewById(R.id.tvProductName);
            tvMeasurement = itemView.findViewById(R.id.tvMeasurement);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvOriginalPrice = itemView.findViewById(R.id.tvOriginalPrice);
        }
    }
}
