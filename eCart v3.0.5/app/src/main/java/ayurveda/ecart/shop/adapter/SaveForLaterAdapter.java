package ayurveda.ecart.shop.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Paint;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import ayurveda.ecart.shop.R;
import ayurveda.ecart.shop.fragment.CartFragment;
import ayurveda.ecart.shop.helper.ApiConfig;
import ayurveda.ecart.shop.helper.Constant;
import ayurveda.ecart.shop.helper.Session;
import ayurveda.ecart.shop.model.Cart;
import ayurveda.ecart.shop.model.CartItems;

@SuppressLint("NotifyDataSetChanged")
public class SaveForLaterAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    // for load more
    public final int VIEW_TYPE_ITEM = 0;
    public final int VIEW_TYPE_LOADING = 1;
    final Activity activity;
    final Session session;
    String taxPercentage;
    ArrayList<Cart> carts;
    ArrayList<Cart> saveForLater;


    public SaveForLaterAdapter(Activity activity, ArrayList<Cart> carts, ArrayList<Cart> saveForLater) {
        this.activity = activity;
        session = new Session(activity);
        this.carts = carts;
        this.saveForLater = saveForLater;
        taxPercentage = "0";
    }

    public void add(int position, Cart item) {
        saveForLater.add(position, item);
        notifyDataSetChanged();
    }

    @SuppressLint("SetTextI18n")
    public void removeItem(Cart cart) {
        CartFragment.isSoldOut = false;

        if (CartFragment.values.containsKey(cart.getProduct_variant_id())) {
            CartFragment.values.replace(cart.getProduct_variant_id(), "0");
        } else {
            CartFragment.values.put(cart.getProduct_variant_id(), "0");
        }

        saveForLater.remove(cart);

        CartFragment.tvSaveForLaterTitle.setText(activity.getResources().getString(R.string.save_for_later) + " (" + getItemCount() + ")");

        CartFragment.tvSaveForLaterTitle.setText(activity.getResources().getString(R.string.save_for_later) + " (" + getItemCount() + ")");
        CartFragment.values.put(cart.getProduct_variant_id(), cart.getQty());
        ApiConfig.AddMultipleProductInCart(session, activity, Constant.CartValues);

        CartFragment.cartAdapter.notifyDataSetChanged();
        Constant.TOTAL_CART_ITEM = CartFragment.cartAdapter.getItemCount();

        CartFragment.SetData(activity);

        if (saveForLater.size() != 0) {
            CartFragment.lytSaveForLater.setVisibility(View.VISIBLE);
        } else {
            CartFragment.lytSaveForLater.setVisibility(View.GONE);
        }

        notifyDataSetChanged();

    }


    @SuppressLint("SetTextI18n")
    public void moveItem(Cart cart) {
        try {
            Constant.FLOAT_TOTAL_AMOUNT = 0;

            CartFragment.cartAdapter.add(cart);

            CartFragment.hashMap.clear();
            for (Cart cart_ : carts) {
                CartItems cartItem = cart_.getItems().get(0);
                long unitMeasurement = (cartItem.getUnit().equalsIgnoreCase("kg") || cartItem.getUnit().equalsIgnoreCase("ltr")) ? 1000 : 1;
                long unit = (long) Double.parseDouble(cartItem.getMeasurement()) * unitMeasurement;

                if (!CartFragment.hashMap.containsKey(cart_.getProduct_id())) {
                    CartFragment.hashMap.put(cart_.getProduct_id(), (long) (Double.parseDouble(cartItem.getStock()) * ((cartItem.getStock_unit_name().equalsIgnoreCase("kg") || cartItem.getStock_unit_name().equalsIgnoreCase("ltr")) ? 1000 : 1) - (unit * Double.parseDouble(cart_.getQty()))));
                } else {
                    long qty = (long) (unit * Double.parseDouble(cart_.getQty()));
                    long available_stock = CartFragment.hashMap.get(cart_.getProduct_id()) - qty;
                    CartFragment.hashMap.replace(cart_.getProduct_id(), available_stock);
                }
            }

            removeItem(cart);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, final int viewType) {
        View view;
        switch (viewType) {
            case (VIEW_TYPE_ITEM):
                view = LayoutInflater.from(activity).inflate(R.layout.lyt_save_for_later, parent, false);
                return new ItemHolder(view);
            case (VIEW_TYPE_LOADING):
                view = LayoutInflater.from(activity).inflate(R.layout.item_progressbar, parent, false);
                return new ViewHolderLoading(view);
            default:
                throw new IllegalArgumentException("unexpected viewType: " + viewType);
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NotNull RecyclerView.ViewHolder holderParent, final int position) {

        int viewType = getItemViewType(position);
        switch (viewType) {
            case VIEW_TYPE_ITEM:

                final ItemHolder holder = (ItemHolder) holderParent;
                final Cart cart = saveForLater.get(position);

                double price;
                double oPrice;

                try {
                    taxPercentage = (Double.parseDouble(cart.getItems().get(0).getTax_percentage()) > 0 ? cart.getItems().get(0).getTax_percentage() : "0");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Picasso.get()
                        .load(cart.getItems().get(0).getImage())
                        .fit()
                        .centerInside()
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder)
                        .into(holder.imgProduct);

                holder.tvDelete.setOnClickListener(v -> removeItem(cart));

                holder.tvAction.setOnClickListener(v -> moveItem(cart));

                holder.tvProductName.setText(cart.getItems().get(0).getName());

                holder.tvMeasurement.setText(cart.getItems().get(0).getMeasurement() + "\u0020" + cart.getItems().get(0).getUnit());

                if (cart.getItems().get(0).getServe_for().equals(Constant.SOLD_OUT_TEXT)) {
                    holder.tvStatus.setVisibility(View.VISIBLE);
                }

                if (cart.getItems().get(0).getDiscounted_price().equals("0") || cart.getItems().get(0).getDiscounted_price().equals("")) {
                    price = ((Float.parseFloat(cart.getItems().get(0).getPrice()) + ((Float.parseFloat(cart.getItems().get(0).getPrice()) * Float.parseFloat(taxPercentage)) / 100)));
                } else {
                    price = ((Float.parseFloat(cart.getItems().get(0).getDiscounted_price()) + ((Float.parseFloat(cart.getItems().get(0).getDiscounted_price()) * Float.parseFloat(taxPercentage)) / 100)));
                    oPrice = ((Float.parseFloat(cart.getItems().get(0).getPrice()) + ((Float.parseFloat(cart.getItems().get(0).getPrice()) * Float.parseFloat(taxPercentage)) / 100)));
                    holder.tvOriginalPrice.setPaintFlags(holder.tvOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    holder.tvOriginalPrice.setText(session.getData(Constant.currency) + ApiConfig.StringFormat("" + oPrice));
                }

                holder.tvPrice.setText(session.getData(Constant.currency) + ApiConfig.StringFormat("" + price));

                CartFragment.lytEmpty.setVisibility(getItemCount() == 0 && carts.size() == 0 ? View.VISIBLE : View.GONE);

                if (carts.size() != 0) {
                    CartFragment.lytTotal.setVisibility(View.VISIBLE);
                } else {
                    CartFragment.lytTotal.setVisibility(View.GONE);
                }

                break;
            case VIEW_TYPE_LOADING:
                ViewHolderLoading loadingViewHolder = (ViewHolderLoading) holderParent;
                loadingViewHolder.progressBar.setIndeterminate(true);
                break;
        }


    }

    @Override
    public int getItemCount() {
        return saveForLater.size();
    }

    @Override
    public int getItemViewType(int position) {
        return saveForLater.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public long getItemId(int position) {
        return Integer.parseInt(carts.get(position).getId());
    }

    static class ViewHolderLoading extends RecyclerView.ViewHolder {
        public final ProgressBar progressBar;

        public ViewHolderLoading(View view) {
            super(view);
            progressBar = view.findViewById(R.id.itemProgressbar);
        }
    }

    public static class ItemHolder extends RecyclerView.ViewHolder {
        final ImageView imgProduct;
        final TextView tvProductName;
        final TextView tvMeasurement;
        final TextView tvPrice;
        final TextView tvOriginalPrice;
        final TextView tvDelete;
        final TextView tvAction;
        final TextView tvStatus;
        final RelativeLayout lytMain;

        public ItemHolder(@NonNull View itemView) {
            super(itemView);
            lytMain = itemView.findViewById(R.id.lytMain);

            imgProduct = itemView.findViewById(R.id.imgProduct);
            tvDelete = itemView.findViewById(R.id.tvDelete);
            tvAction = itemView.findViewById(R.id.tvAction);
            tvStatus = itemView.findViewById(R.id.tvStatus);

            tvProductName = itemView.findViewById(R.id.tvProductName);
            tvMeasurement = itemView.findViewById(R.id.tvMeasurement);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvOriginalPrice = itemView.findViewById(R.id.tvOriginalPrice);
        }
    }
}
