package ayurveda.ecart.shop.adapter;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;

import ayurveda.ecart.shop.R;
import ayurveda.ecart.shop.activity.MainActivity;
import ayurveda.ecart.shop.fragment.ProductListFragment;
import ayurveda.ecart.shop.helper.ApiConfig;
import ayurveda.ecart.shop.helper.Constant;
import ayurveda.ecart.shop.model.Product;
import ayurveda.ecart.shop.model.Section;
import ayurveda.ecart.shop.model.Variants;

public class SectionAdapter extends RecyclerView.Adapter<SectionAdapter.SectionHolder> {

    public final ArrayList<Section> sectionList;
    public final Activity activity;
    public final JSONArray jsonArray;
    HashMap<String, Long> hashMap;
    JSONArray jsonArrayImages;

    public SectionAdapter(Activity activity, ArrayList<Section> sectionList, JSONArray jsonArray) {
        this.activity = activity;
        this.sectionList = sectionList;
        this.jsonArray = jsonArray;
        this.hashMap = new HashMap<>();
        jsonArrayImages = new JSONArray();
    }

    @Override
    public int getItemCount() {
        return sectionList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull SectionHolder holder, final int position) {
        try {
            Section section = sectionList.get(position);
            holder.tvTitle.setText(section.getTitle());
            holder.tvSubTitle.setText(section.getShort_description());

            holder.lytBelowSectionOfferImages.setLayoutManager(new LinearLayoutManager(activity));
            holder.lytBelowSectionOfferImages.setNestedScrollingEnabled(false);

            try {
                jsonArrayImages = jsonArray.getJSONObject(position).getJSONArray(Constant.OFFER_IMAGES);
            } catch (Exception e) {
                e.printStackTrace();
            }

            for (Product product : section.getProducts()) {
                for (Variants variant : product.getVariants()) {
                    long unitMeasurement = (variant.getMeasurement_unit_name().equalsIgnoreCase("kg") || variant.getMeasurement_unit_name().equalsIgnoreCase("ltr")) ? 1000 : 1;
                    long unit = (long) Double.parseDouble(variant.getMeasurement()) * unitMeasurement;

                    if (!hashMap.containsKey(variant.getProduct_id())) {
                        hashMap.put(variant.getProduct_id(), (long) (Double.parseDouble(variant.getStock()) * ((variant.getStock_unit_name().equalsIgnoreCase("kg") || variant.getStock_unit_name().equalsIgnoreCase("ltr")) ? 1000 : 1) - (unit * Long.parseLong(variant.getCart_count()))));
                    } else {
                        hashMap.replace(variant.getProduct_id(), hashMap.get(variant.getProduct_id()) - (unit * Long.parseLong(variant.getCart_count())));
                    }
                }
            }

            switch (section.getStyle()) {
                case "style_1":
                    ApiConfig.GetOfferImage(jsonArrayImages, holder.lytBelowSectionOfferImages);
                    holder.recyclerView.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
                    AdapterStyle1 adapter = new AdapterStyle1(activity, section.getProducts(), R.layout.offer_layout, hashMap);
                    holder.recyclerView.setAdapter(adapter);

                    break;
                case "style_2":
                    ApiConfig.GetOfferImage(jsonArrayImages, holder.lytBelowSectionOfferImages);
                    holder.recyclerView.setLayoutManager(new LinearLayoutManager(activity));
                    AdapterStyle2 adapterStyle2 = new AdapterStyle2(activity, section.getProducts(), hashMap);
                    holder.recyclerView.setAdapter(adapterStyle2);
                    break;
                case "style_3":
                    ApiConfig.GetOfferImage(jsonArrayImages, holder.lytBelowSectionOfferImages);
                    holder.recyclerView.setLayoutManager(new GridLayoutManager(activity, 2));
                    AdapterStyle1 adapter3 = new AdapterStyle1(activity, section.getProducts(), R.layout.lyt_style_3, hashMap);
                    holder.recyclerView.setAdapter(adapter3);
                    break;
            }

            holder.tvMore.setOnClickListener(view -> {

                Fragment fragment = new ProductListFragment();
                Bundle bundle = new Bundle();
                bundle.putString(Constant.FROM, "section");
                bundle.putString(Constant.NAME, section.getTitle());
                bundle.putString(Constant.ID, section.getId());
                fragment.setArguments(bundle);

                MainActivity.fm.beginTransaction().add(R.id.container, fragment).addToBackStack(null).commit();
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @NonNull
    @Override
    public SectionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.section_layout, parent, false);
        return new SectionHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public static class SectionHolder extends RecyclerView.ViewHolder {
        final TextView tvTitle;
        final TextView tvSubTitle;
        final TextView tvMore;
        final RecyclerView recyclerView, lytBelowSectionOfferImages;

        public SectionHolder(View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvSubTitle = itemView.findViewById(R.id.tvSubTitle);
            tvMore = itemView.findViewById(R.id.tvMore);
            recyclerView = itemView.findViewById(R.id.recyclerView);

            lytBelowSectionOfferImages = itemView.findViewById(R.id.lytBelowSectionOfferImages);

        }
    }


}
