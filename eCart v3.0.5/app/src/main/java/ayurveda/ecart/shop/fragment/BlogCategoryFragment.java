package ayurveda.ecart.shop.fragment;

import static android.content.Context.INPUT_METHOD_SERVICE;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ayurveda.ecart.shop.R;
import ayurveda.ecart.shop.adapter.BlogCategoryAdapter;
import ayurveda.ecart.shop.helper.ApiConfig;
import ayurveda.ecart.shop.model.BlogCategory;
import ayurveda.ecart.shop.helper.Constant;


public class BlogCategoryFragment extends Fragment {

    public static ArrayList<BlogCategory> categoryArrayList;
    public static BlogCategoryAdapter blogCategoryAdapter;
    TextView tvNoData;
    RecyclerView recyclerView;
    SwipeRefreshLayout swipeLayout;
    View root;
    Activity activity;
    private ShimmerFrameLayout mShimmerViewContainer;
    NestedScrollView nestedScrollView;
    int total;
    int offset = 0;
    boolean isLoadMore = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_blog_category, container, false);
        mShimmerViewContainer = root.findViewById(R.id.mShimmerViewContainer);

        activity = getActivity();

        setHasOptionsMenu(true);


        tvNoData = root.findViewById(R.id.tvNoData);
        swipeLayout = root.findViewById(R.id.swipeLayout);
        recyclerView = root.findViewById(R.id.recyclerView);
        nestedScrollView = root.findViewById(R.id.nestedScrollView);

        recyclerView.setLayoutManager(new GridLayoutManager(activity, Constant.GRID_COLUMN));
        swipeLayout.setColorSchemeColors(ContextCompat.getColor(activity, R.color.colorPrimary));


        swipeLayout.setOnRefreshListener(() -> {
            swipeLayout.setRefreshing(false);
            if (ApiConfig.isConnected(activity)) {
                recyclerView.setVisibility(View.GONE);
                mShimmerViewContainer.setVisibility(View.VISIBLE);
                mShimmerViewContainer.startShimmer();
                getBlogs();
            }
        });

        if (ApiConfig.isConnected(activity)) {
            recyclerView.setVisibility(View.GONE);
            mShimmerViewContainer.setVisibility(View.VISIBLE);
            mShimmerViewContainer.startShimmer();
            getBlogs();
        }

        return root;
    }


    public void startShimmer() {
        mShimmerViewContainer.setVisibility(View.VISIBLE);
        mShimmerViewContainer.startShimmer();
    }

    public void stopShimmer() {
        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);
    }

    void getBlogs() {
        categoryArrayList = new ArrayList<>();
        startShimmer();

        Map<String, String> params = new HashMap<>();
        params.put(Constant.GET_BLOG_CATEGORIES, Constant.GetVal);
        params.put(Constant.LIMIT, "" + Constant.LOAD_ITEM_LIMIT);
        params.put(Constant.OFFSET, "" + offset);

        ApiConfig.RequestToVolley((result, response) -> {
            if (result) {
                try {
                    JSONObject jsonObject1 = new JSONObject(response);
                    if (!jsonObject1.getBoolean(Constant.ERROR)) {
                        total = Integer.parseInt(jsonObject1.getString(Constant.TOTAL));
                        JSONObject object = new JSONObject(response);
                        JSONArray jsonArray = object.getJSONArray(Constant.DATA);
                        try {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                BlogCategory blogCategory = new Gson().fromJson(jsonArray.getJSONObject(i).toString(), BlogCategory.class);
                                categoryArrayList.add(blogCategory);
                            }
                        } catch (Exception e) {
                            stopShimmer();
                            recyclerView.setVisibility(View.GONE);
                            tvNoData.setVisibility(View.GONE);
                        }
                        if (offset == 0) {
                            blogCategoryAdapter = new BlogCategoryAdapter(activity, categoryArrayList);
                            recyclerView.setAdapter(blogCategoryAdapter);
                            stopShimmer();
                            recyclerView.setVisibility(View.VISIBLE);
                            tvNoData.setVisibility(View.GONE);
                            nestedScrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
                                // if (diff == 0) {
                                if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
                                    LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                                    if (categoryArrayList.size() < total) {
                                        if (!isLoadMore) {
                                            if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == categoryArrayList.size() - 1) {
                                                //bottom of list!
                                                categoryArrayList.add(null);
                                                blogCategoryAdapter.notifyItemInserted(categoryArrayList.size() - 1);
                                                offset += Integer.parseInt("" + Constant.LOAD_ITEM_LIMIT);

                                                Map<String, String> params1 = new HashMap<>();
                                                params1.put(Constant.GET_BLOG_CATEGORIES, Constant.GetVal);
                                                params1.put(Constant.LIMIT, "" + Constant.LOAD_ITEM_LIMIT);
                                                params1.put(Constant.OFFSET, "" + offset);

                                                ApiConfig.RequestToVolley((result1, response1) -> {
                                                    if (result1) {
                                                        categoryArrayList.remove(categoryArrayList.size() - 1);
                                                        blogCategoryAdapter.notifyItemRemoved(categoryArrayList.size());
                                                        try {
                                                            JSONObject jsonObject11 = new JSONObject(response1);
                                                            if (!jsonObject11.getBoolean(Constant.ERROR)) {
                                                                JSONObject object1 = new JSONObject(response1);
                                                                JSONArray jsonArray1 = object1.getJSONArray(Constant.DATA);
                                                                for (int i = 0; i < jsonArray1.length(); i++) {
                                                                    BlogCategory blogCategory = new Gson().fromJson(jsonArray1.getJSONObject(i).toString(), BlogCategory.class);
                                                                    categoryArrayList.add(blogCategory);
                                                                }
                                                                blogCategoryAdapter.notifyDataSetChanged();
                                                                isLoadMore = false;
                                                            }
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                }, activity, Constant.GET_BLOGS_URL, params1, false);
                                                isLoadMore = true;
                                            }
                                        }
                                    }
                                }
                            });
                        }
                    } else {
                        stopShimmer();
                        recyclerView.setVisibility(View.GONE);
                        tvNoData.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopShimmer();
                    recyclerView.setVisibility(View.GONE);
                    tvNoData.setVisibility(View.GONE);
                }
            }
        }, activity, Constant.GET_BLOGS_URL, params, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        Constant.TOOLBAR_TITLE = getString(R.string.blogs_categories);
        requireActivity().invalidateOptionsMenu();
        hideKeyboard();
    }

    public void hideKeyboard() {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
            assert inputMethodManager != null;
            inputMethodManager.hideSoftInputFromWindow(root.getApplicationWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.toolbar_layout).setVisible(false);
        menu.findItem(R.id.toolbar_cart).setVisible(true);
        menu.findItem(R.id.toolbar_sort).setVisible(false);
        menu.findItem(R.id.toolbar_search).setVisible(true);
    }
}
