package ayurveda.ecart.shop.fragment;

import static android.content.Context.INPUT_METHOD_SERVICE;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.squareup.picasso.Picasso;

import ayurveda.ecart.shop.R;
import ayurveda.ecart.shop.model.Blog;
import ayurveda.ecart.shop.helper.Constant;


public class BlogFragment extends Fragment {

    WebView webViewBlog;
    ImageView imageView;
    View root;
    Activity activity;
    Blog blog;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_blog, container, false);

        activity = getActivity();

        setHasOptionsMenu(true);

        webViewBlog = root.findViewById(R.id.webViewBlog);
        imageView = root.findViewById(R.id.imageView);

        blog = (Blog) getArguments().getSerializable("model");

        Picasso.get()
                .load(blog.getImage())
                .fit()
                .centerInside()
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(imageView);

        webViewBlog.getSettings().setJavaScriptEnabled(true);

        webViewBlog.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest webResourceRequest) {
                if (webResourceRequest.getUrl() != null) {
                    view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(webResourceRequest.getUrl().toString())));
                    return true;
                } else {
                    return false;
                }
            }
        });

        webViewBlog.setVerticalScrollBarEnabled(true);
        webViewBlog.loadDataWithBaseURL("", blog.getDescription(), "text/html", "UTF-8", "");



        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        Constant.TOOLBAR_TITLE = blog.getTitle();
        requireActivity().invalidateOptionsMenu();
        hideKeyboard();
    }

    public void hideKeyboard() {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
            assert inputMethodManager != null;
            inputMethodManager.hideSoftInputFromWindow(root.getApplicationWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.toolbar_layout).setVisible(false);
        menu.findItem(R.id.toolbar_cart).setVisible(true);
        menu.findItem(R.id.toolbar_sort).setVisible(false);
        menu.findItem(R.id.toolbar_search).setVisible(true);
    }
}
