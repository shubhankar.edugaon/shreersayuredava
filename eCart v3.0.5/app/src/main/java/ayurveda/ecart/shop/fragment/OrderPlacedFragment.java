package ayurveda.ecart.shop.fragment;

import static android.content.Context.INPUT_METHOD_SERVICE;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.airbnb.lottie.LottieAnimationView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ayurveda.ecart.shop.R;
import ayurveda.ecart.shop.activity.MainActivity;
import ayurveda.ecart.shop.helper.ApiConfig;
import ayurveda.ecart.shop.helper.Constant;
import ayurveda.ecart.shop.helper.Session;

public class OrderPlacedFragment extends Fragment {
    View root;
    Activity activity;
    ProgressBar progressBar;
    Button btnShopping, btnSummary;
    LottieAnimationView lottieAnimationView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_order_placed, container, false);
        activity = getActivity();
        Session session = new Session(activity);
        progressBar = root.findViewById(R.id.progressBar);
        btnShopping = root.findViewById(R.id.btnShopping);
        btnSummary = root.findViewById(R.id.btnSummary);
        lottieAnimationView = root.findViewById(R.id.lottieAnimationView);
        setHasOptionsMenu(true);

        btnShopping.setEnabled(false);
        btnSummary.setEnabled(false);

        RemoveAllItemFromCart(activity, session);

        return root;
    }

    public void RemoveAllItemFromCart(final Activity activity, Session session) {
        progressBar.setVisibility(View.VISIBLE);
        Map<String, String> params = new HashMap<>();
        params.put(Constant.REMOVE_FROM_CART, Constant.GetVal);
        if (session.getBoolean(Constant.IS_USER_LOGIN))
            params.put(Constant.USER_ID, session.getData(Constant.ID));

        ApiConfig.RequestToVolley((result, response) -> {
            if (result) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (!jsonObject.getBoolean(Constant.ERROR)) {
                        getCartItemCount(activity, session);
                    }
                    progressBar.setVisibility(View.GONE);
                } catch (JSONException e) {
                    e.printStackTrace();
                    progressBar.setVisibility(View.GONE);
                }
            }
        }, activity, Constant.CART_URL, params, false);
    }


    public void getCartItemCount(final Activity activity, Session session) {
        Map<String, String> params = new HashMap<>();
        params.put(Constant.GET_USER_CART, Constant.GetVal);
        if (session.getBoolean(Constant.IS_USER_LOGIN))
            params.put(Constant.USER_ID, session.getData(Constant.ID));

        ApiConfig.RequestToVolley((result, response) -> {
            if (result) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (!jsonObject.getBoolean(Constant.ERROR)) {
                        Constant.TOTAL_CART_ITEM = Integer.parseInt(jsonObject.getString(Constant.TOTAL));
                    } else {
                        Constant.TOTAL_CART_ITEM = 0;
                    }

                    Constant.CartValues.clear();
                    lottieAnimationView.playAnimation();

                    btnShopping.setBackgroundTintList(ContextCompat.getColorStateList(activity,R.color.colorPrimary));
                    btnSummary.setBackgroundTintList(ContextCompat.getColorStateList(activity,R.color.colorPrimary));

                    btnShopping.setEnabled(true);
                    btnSummary.setEnabled(true);

                    btnShopping.setOnClickListener(view -> {
                        startActivity(new Intent(activity, MainActivity.class).putExtra(Constant.FROM, "").addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        activity.finish();
                    });
                    btnSummary.setOnClickListener(v -> {
                        startActivity(new Intent(activity, MainActivity.class).putExtra(Constant.FROM, "tracker"));
                        activity.finish();
                    });

                    activity.invalidateOptionsMenu();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, activity, Constant.CART_URL, params, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.toolbar.setVisibility(View.GONE);
        lottieAnimationView.setAnimation("placed-order.json");
        activity.invalidateOptionsMenu();
        hideKeyboard();
    }

    public void hideKeyboard() {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
            assert inputMethodManager != null;
            inputMethodManager.hideSoftInputFromWindow(root.getApplicationWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        menu.findItem(R.id.toolbar_layout).setVisible(false);
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.toolbar_cart).setVisible(false);
        menu.findItem(R.id.toolbar_sort).setVisible(false);
        menu.findItem(R.id.toolbar_search).setVisible(false);
    }

}
