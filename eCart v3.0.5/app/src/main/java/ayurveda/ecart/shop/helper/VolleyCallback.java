package ayurveda.ecart.shop.helper;

public interface VolleyCallback {
    void onSuccess(boolean result, String message);
}
