package ayurveda.ecart.shop.model;

import java.io.Serializable;

public class Address implements Serializable {

    String id;
    String user_id;
    String type;
    String name;
    String mobile;
    String alternate_mobile;
    String address;
    String landmark;
    String area_id;
    String city_id;
    String pincode;
    String state;
    String country;
    String city_name;
    String area_name;
    String is_default;
    String latitude;
    String longitude;
    String minimum_free_delivery_order_amount;
    String delivery_charges;
    String minimum_order_amount;

    public Address() {
    }

    public String getCity_id() {
        return city_id;
    }

    public String getMinimum_order_amount() {
        return minimum_order_amount;
    }

    public String getMinimum_free_delivery_order_amount() {
        return minimum_free_delivery_order_amount;
    }

    public String getDelivery_charges() {
        return delivery_charges;
    }


    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }


    public String getIs_default() {
        return is_default;
    }

    public void setIs_default(String is_default) {
        this.is_default = is_default;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAlternate_mobile() {
        return alternate_mobile;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLandmark() {
        return landmark;
    }

    public String getArea_id() {
        return area_id;
    }

    public String getPincode() {
        return pincode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public String getCity_name() {
        return city_name;
    }

    public String getArea_name() {
        return area_name;
    }

}
