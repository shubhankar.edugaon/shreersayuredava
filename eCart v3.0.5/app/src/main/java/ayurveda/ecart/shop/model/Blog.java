package ayurveda.ecart.shop.model;

import java.io.Serializable;

public class Blog implements Serializable {
    String id,title,description,image;

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }
}
