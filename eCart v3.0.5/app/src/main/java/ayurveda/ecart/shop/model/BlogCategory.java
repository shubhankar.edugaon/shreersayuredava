package ayurveda.ecart.shop.model;

import java.io.Serializable;

public class BlogCategory implements Serializable {
    String id,name,image;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }
}
