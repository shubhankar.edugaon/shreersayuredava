package ayurveda.ecart.shop.model;

import java.io.Serializable;


public class OfflineItems implements Serializable {

    String id;
    String product_id;
    String type;
    String measurement;
    String price;
    String discounted_price;
    String serve_for;
    String stock;
    String name;
    String image;
    String unit;
    String cart_count;
    String stock_unit_name;
    String total_allowed_quantity;

    public String getTotal_allowed_quantity() {
        return total_allowed_quantity;
    }

    public void setCart_count(String cart_count) {
        this.cart_count = cart_count;
    }

    public String getId() {
        return id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public String getType() {
        return type;
    }

    public String getMeasurement() {
        return measurement;
    }

    public String getPrice() {
        return price;
    }

    public String getDiscounted_price() {
        return discounted_price;
    }

    public String getServe_for() {
        return serve_for;
    }

    public void setServe_for(String serve_for) {
        this.serve_for = serve_for;
    }

    public String getStock() {
        return stock;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public String getUnit() {
        return unit;
    }

    public String getCart_count() {
        return cart_count;
    }

    public String getStock_unit_name() {
        return stock_unit_name;
    }
}
