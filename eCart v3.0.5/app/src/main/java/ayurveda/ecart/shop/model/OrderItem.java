package ayurveda.ecart.shop.model;

import java.io.Serializable;

public class OrderItem implements Serializable {
    String id;
    String user_id;
    String order_id;
    String product_variant_id;
    String quantity;
    String price;
    String discounted_price;
    String tax_percentage;
    String discount;
    String product_id;
    String variant_id;
    String rate;
    String review;
    String name;
    String image;
    String return_status;
    String cancelable_status;
    String till_status;
    String measurement;
    String unit;
    String active_status;
    boolean review_status;

    public String getActive_status() {
        return active_status;
    }

    public void setActive_status(String active_status) {
        this.active_status = active_status;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public void setReview_status(boolean review_status) {
        this.review_status = review_status;
    }

    public boolean isReview_status() {
        return review_status;
    }

    public String getId() {
        return id;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public String getProduct_variant_id() {
        return product_variant_id;
    }

    public String getQuantity() {
        return quantity;
    }

    public String getPrice() {
        return price;
    }

    public String getDiscounted_price() {
        return discounted_price;
    }

    public String getTax_percentage() {
        return tax_percentage;
    }

    public String getDiscount() {
        return discount;
    }

    public String getProduct_id() {
        return product_id;
    }

    public String getVariant_id() {
        return variant_id;
    }

    public String getRate() {
        return rate;
    }

    public String getReview() {
        return review;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public String getReturn_status() {
        return return_status;
    }

    public String getCancelable_status() {
        return cancelable_status;
    }

    public String getTill_status() {
        return till_status;
    }

    public String getMeasurement() {
        return measurement;
    }

    public String getUnit() {
        return unit;
    }
}
