package ayurveda.ecart.shop.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Product implements Serializable {

    String id;
    String name;
    String slug;
    String category_id;
    String indicator;
    String manufacturer;
    String made_in;
    String return_status;
    String cancelable_status;
    String till_status;
    String image;
    String size_chart;
    String description;
    String status;
    String ratings;
    String number_of_ratings;
    String tax_percentage;
    String total_allowed_quantity;
    String shipping_delivery;
    String image_mode;
    boolean is_favorite;
    ArrayList<Variants> variants;
    ArrayList<String> other_images;

    public String getImage_mode() {
        return image_mode;
    }

    public String getShipping_delivery() {
        return shipping_delivery;
    }

    public void setIs_favorite(boolean is_favorite) {
        this.is_favorite = is_favorite;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSlug() {
        return slug;
    }

    public String getCategory_id() {
        return category_id;
    }

    public String getIndicator() {
        return indicator;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getMade_in() {
        return made_in;
    }

    public String getReturn_status() {
        return return_status;
    }

    public String getCancelable_status() {
        return cancelable_status;
    }

    public String getTill_status() {
        return till_status;
    }

    public String getImage() {
        return image;
    }

    public String getSize_chart() {
        return size_chart;
    }

    public String getDescription() {
        return description;
    }

    public String getStatus() {
        return status;
    }

    public String getRatings() {
        return ratings;
    }

    public String getNumber_of_ratings() {
        return number_of_ratings;
    }

    public String getTax_percentage() {
        return tax_percentage;
    }

    public String getTotal_allowed_quantity() {
        return total_allowed_quantity;
    }

    public boolean isIs_favorite() {
        return is_favorite;
    }

    public ArrayList<Variants> getVariants() {
        return variants;
    }

    public ArrayList<String> getOther_images() {
        return other_images;
    }
}
