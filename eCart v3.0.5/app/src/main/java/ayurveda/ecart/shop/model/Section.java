package ayurveda.ecart.shop.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Section implements Serializable {

    String id,title,short_description,style;
    ArrayList<Product> products;
    ArrayList<Image> offer_images;

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getShort_description() {
        return short_description;
    }

    public String getStyle() {
        return style;
    }

    public ArrayList<Image> getOffer_images() {
        return offer_images;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }
}
